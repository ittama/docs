const moment = require("moment");

module.exports = {
  locales: {
    "/": {
      lang: "id-ID", // this will be set as the lang attribute on <html>
      title: "PERFORMA BNN v1.0",
      description: "",
    },
  },
  markdown: {
    config: (md) => {
      md.use(require("markdown-it-deflist"));
    },
    extendMarkdown: (md) => {
      md.set({
        breaks: true,
      });
      md.use(require("markdown-it-imsize"));
    },
  },
  plugins: [
    // you can use this plugin multiple times
    ["@vuepress/plugin-medium-zoom", true],
    ["@vuepress/back-to-top", true],
    [
      "vuepress-plugin-container",
      {
        type: "right",
        defaultTitle: "",
      },
    ],
    [
      "vuepress-plugin-container",
      {
        type: "theorem",
        before: (info) => `<div class="theorem"><p class="title">${info}</p>`,
        after: "</div>",
      },
    ],

    // this is how VuePress Default Theme use this plugin
    [
      "vuepress-plugin-container",
      {
        type: "tip",
        defaultTitle: {
          "/": "TIP",
          "/zh/": "提示",
        },
      },
    ],
    [
      "@vuepress/last-updated",
      {
        transformer: (timestamp, lang) => {
          // Don't forget to install moment yourself
          const moment = require("moment");
          moment.locale(lang);
          return moment(timestamp).fromNow();
        },
      },
    ],
    ['@snowdog/vuepress-plugin-pdf-export', {
      puppeteerLaunchOptions: {
        args: ['--no-sandbox', '--disable-setuid-sandbox']
      }
    }],
  ],
  title: "PERFORMA v1.0",
  description: "PETUNJUK PENGGUNAAN APLIKASI",
  base: "/docs/",
  dest: "public",
  themeConfig: {
    logo: "/assets/img/performa_bar.png",
    nav: [
      {
        text: "MOBILE",
        link: "/android/home",
      },
      {
        text: "PERPUSTAKAAN PEGAWAI",
        link: "/nm/home",
      },
      {
        text: "LAYANAN PEGAWAI",
        link: "/pm/sprin",
      },
    ],
    sidebar: [
      {
        title: "Login dan Menu",
        collapsable: false,
        children: [
          "/lm/login",
          "/lm/menu",
        ],
      },
      {
        title: "Perpustakaan Pegawai",
        collapsable: false,
        children: [
          "/nm/home",
          "/nm/profil",
          "/nm/kompetensi",
          "/nm/kesejahteraan",
          "nm/kinerja",
        ],
      },
      {
        title: "Layanan Kepegawaian",
        collapsable: false,
        children: [
          "/pm/sprin",
          "/pm/penomoran",
          "/pm/spd",
          "/pm/nd",
          "/pm/cuti",
        ],
      },
      {
        title: "Layanan Perkantoran",
        collapsable: false,
        children: [
          // "/km/atk", 
          // "/km/harwat",
          "/km/jilid",
          "/km/konsumsi",
          "/km/rekap"
        ],
      },
      {
        title: "Layanan Mandiri",
        collapsable: false,
        children: [
          "/android/home",
          "/android/dashboard",
          "/android/diklat",
          "/android/sejahtera",
          // "/android/profil",
        ],
      },
    ],
    lastUpdated: "Terakhir di Update", // string | boolean
  },
};
