---
home: true
actionText: Selengkapnya
actionLink: /lm/login
heroImage: /assets/img/performa_login.png 
heroText:
features:
  - title: PERPUSTAKAAN PEGAWAI
    details: Perpustakaan pegawai merupakan kumpulan data pegawai berupa dokumen-dokumen Surat Keputusan,  Riwayat Jabatan (Mutasi), Riwayat Pangkat, Riwayat Kenaikan Gaji Berkala, Riwayat cuti dan Sertifikat serta Ijasah Pendidikan dan Pelatihan (Diklat) setiap pegawai.
  - title: PELAYANAN PEGAWAI
    details: Pelayanan pegawai merupakan layanan pengajuan Cuti, Konsumsi rapat, Ruang Rapat, ATK, Harwat, Penjilidan Laporan, Perjalanan dinas dan Surat Perintah/Penugasan serta pwnyimpanan seluruh dokumen pengajuan-pengajuan dan sprin-sprin tersebut.
  - title: LAYANAN MANDIRI
    details: Layanan Mandiri merupakan Aplikasi Mobile yang dapat digunakan oleh seluruh pegawai untuk menampilkan Dokumen-dokumen personal seperti riwayat Cuti, Riwayat Jabatan (Mutasi), Riwayat Pangkat serta Riwayat Gaji Berkala, dan dapat juga mengajukan terkait cuti, Konsumsi Rapat, Ruang Rapat, ATK, Harwat, Penjilidan Laporan, Perjalanan Dinas (SPD) dan Surat Perintah (Sprin)/Penugasan. 
footer: Sistem Pelayanan dan Kinerja Pegawai ITTAMA BNN v1.0 @2020
---

## Fitur

Menyedia Sistem Layanan Mandiri Pegawai yang menyediakan fasilitas pengelolaan data kepegawaian di Inspektorat Utama BNN untuk melaksanakan kegiatan adminstratif dalam menunjang kebijakan-kebijakan yang berkaitan dengan masalah kepegawaian di lingkungannya, termasuk di dalamnya Aplikasi untuk Administrator/Operator dan Aplikasi Mobile untuk pegawai

## Fungsi

- Meningkatkan kinerja organisasi dan aparatur
- Menjadi salah satu instrumen dalam penataan dan penyempurnaan organisasi
- Menjadi alat ukur prestasi kerja organisasi dan aparatur
- Meningkatkan kesejahteraan aparatur
- Meningkatkan motivasi kerja ASN
- Meningkatkan disiplin kerja ASN
- Mendorong terciptanya kompetisi yang sehat di antara aparatur
- Meningkatkan kompetensi ASN
- Menumbuhkan kreativitas dan inovasi kerja yang lebih tinggi

<my-header></my-header>
