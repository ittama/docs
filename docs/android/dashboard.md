---
title: Dashboard
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# DASHBOARD
[[toc]]
Halaman ini berisikan menu-menu untuk Layanan Kepegawaian dan Layanan Perkantoran seperti :
- Penjilidan Laporan
- Konsumsi Rapat
- Sprin/Penugasan
- Perjalanan Dinas (SPD)
- Penomoran Laporan
- Nota Dinas (ND) Diklat

<kbd>
  <img :src="$withBase('/assets/img/android/dashboard.png')" alt="TampilanDashboard">
</kbd>

## Notifikasi

Pada bagian kanan atas layar terdapat icon ![Magic](https://img.shields.io/badge/notifikasi-orange?&logo=data:image/svg%2bxml;base64,PHN2ZyBpZD0iTGF5ZXJfNCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMjQgMjQiIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgMjQgMjQiIHdpZHRoPSI1MTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGc+PHBhdGggZD0ibTIxLjM3OSAxNi45MTNjLTEuNTEyLTEuMjc4LTIuMzc5LTMuMTQ2LTIuMzc5LTUuMTI1di0yLjc4OGMwLTMuNTE5LTIuNjE0LTYuNDMyLTYtNi45MnYtMS4wOGMwLS41NTMtLjQ0OC0xLTEtMXMtMSAuNDQ3LTEgMXYxLjA4Yy0zLjM4Ny40ODgtNiAzLjQwMS02IDYuOTJ2Mi43ODhjMCAxLjk3OS0uODY3IDMuODQ3LTIuMzg4IDUuMTMzLS4zODkuMzMzLS42MTIuODE3LS42MTIgMS4zMjkgMCAuOTY1Ljc4NSAxLjc1IDEuNzUgMS43NWgxNi41Yy45NjUgMCAxLjc1LS43ODUgMS43NS0xLjc1IDAtLjUxMi0uMjIzLS45OTYtLjYyMS0xLjMzN3oiLz48cGF0aCBkPSJtMTIgMjRjMS44MTEgMCAzLjMyNi0xLjI5MSAzLjY3NC0zaC03LjM0OGMuMzQ4IDEuNzA5IDEuODYzIDMgMy42NzQgM3oiLz48L2c+PC9zdmc+) pada sisi atas ikon terdapat pemberitahuan jumlah notifikasi yang belum dibaca.

<kbd>
  <img :src="$withBase('/assets/img/android/notif_screen.png')" alt="TampilanNotifikasi">
</kbd>

---

## Penjilidan Laporan

Merupakan proses permintaan untuk melakukan penjilidan laporan berdasarkan kegiatan yang dijalankan sesuai dengan Surat Perintah, terdapat informasi bentuk jilid, jumlah jilid dan batas waktu penyelesaian penjilidan laporan.

<kbd>
  <img :src="$withBase('/assets/img/android/jilid_screen.png')" alt="TampilanJilid">
</kbd>

### Tambah Pengajuan Jilid

Tekan tombol `+` pada bagian kanan layar, akan tampil layar tambah data pengajuan seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/android/jilid_add.png')" alt="TampilanJilidAdd">
</kbd>

Field Label                     | Isian 
--  | - |
`Pilih Laporan` | Pilih terlebih dahulu Nomor Laporan  ![Wajib Pilih !](https://img.shields.io/badge/Wajib-Pilih-1abc9c.svg)
`Nama Dokumen` | Nama dokumen akan otomatis terisi setelah Nomor Laporan di atas di pilih <Badge text="wajib diisikan" type="error" />, Nama dokumen dapat di ubah dan disesuaikan
`Spiral`    | Isikan jumlah jilid dengan bentuk Spiral
`Hard`    | Isikan jumlah jilid dengan bentuk Hard cover
`Biasa`    | Isikan jumlah jilid dengan bentuk Biasa
`Tanggal_Selesai`    | Masukan tanggal penyelesaian penjilidan
![Save](https://img.shields.io/badge/simpan-orange?&logo=data:image/svg%2bxml;base64,PHN2ZyBpZD0iQ2FwYV8xIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIuMDA3IDUxMi4wMDciIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgNTEyLjAwNyA1MTIuMDA3IiB3aWR0aD0iNTEyIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnPjxwYXRoIGQ9Im0xNDIgMTQzLjAwM2gxMzl2LTE0M2MtNDkuMzc5IDAtMTA3LjY3NCAwLTE1NCAwdjEyOGMwIDguMjcxIDYuNzI5IDE1IDE1IDE1eiIvPjxwYXRoIGQ9Im0zNDUgMTI4LjAwM3YtMTI4Yy05Ljk3IDAtMjEuNDYxIDAtMzQgMHYxNDNoMTljOC4yNzEgMCAxNS02LjcyOSAxNS0xNXoiLz48cGF0aCBkPSJtMTI3IDUxMi4wMDNoMjE4di0xNjFoLTIxOHoiLz48cGF0aCBkPSJtNTExLjkyNyAxMjYuNTM3Yy0uMjc5LTIuODI4LTEuMzgtNS42NjctMy4zMTUtOC4wMjctLjc0Ny0uOTEzIDYuODkzIDYuNzg2LTExNC4wMDYtMTE0LjExMy0yLjg4Mi0yLjg4Mi02Ljc5NC00LjM5Ni0xMC42MTItNC4zOTQtLjc4OSAwLTMuOTI1IDAtOC45OTUgMHYxMjhjMCAyNC44MTMtMjAuMTg3IDQ1LTQ1IDQ1LTE0LjAyOCAwLTE4Ni4wNjQgMC0xODggMC0yNC44MTMgMC00NS0yMC4xODctNDUtNDV2LTEyOGMtMjkuNTg5IDAtNDkuODIgMC01MiAwLTI0LjgxMyAwLTQ1IDIwLjE4Ny00NSA0NXY0MjJjMCAyNC44MTMgMjAuMTg3IDQ1IDQ1IDQ1aDUyYzAtMTAuODE1IDAtMjAxLjc5NyAwLTIxMCAwLTI0LjgxMyAyMC4xODctNDUgNDUtNDVoMTg4YzI0LjgxMyAwIDQ1IDIwLjE4NyA0NSA0NXYyMTBoOTJjMjQuODEzIDAgNDUtMjAuMTg3IDQ1LTQ1IC4wMDEtMzY0LjE4Ni4wNDEtMzM5LjMxNi0uMDcyLTM0MC40NjZ6Ii8+PHBhdGggZD0ibTMzMCAyODcuMDAzaC0xODhjLTguMjcxIDAtMTUgNi43MjktMTUgMTV2MTloMjE4di0xOWMwLTguMjcxLTYuNzI5LTE1LTE1LTE1eiIvPjwvZz48L3N2Zz4=) | Untuk menyimpan data, tekan tombol simpan pada bagian atas kanan layar

### Ubah Pengajuan

Pada layar Penjilidan Laporan, swipe ke kiri data yang akan kita ubah isiannya 

<kbd>
  <img :src="$withBase('/assets/img/android/jilid_screen_cardmenu.png')" alt="TampilanJilidScreenCardMenu">
</kbd>

Lalu tekan tombol `Ubah`, tampilan form untuk ubah data pengajuan sebagai berikut :

<kbd>
  <img :src="$withBase('/assets/img/android/jilid_ubah.png')" alt="TampilanJilidUbah">
</kbd>

---

## Konsumsi Rapat

Konsumsi Rapat adalah pengajuan penggunaan ruang rapat serta keperluan rapat lainnya termasuk konsumsi untuk rapat tersebut, pengajuan rapat harus dilakukan 1 hari sebelum hari pelaksanaan.

> **_Catatan:_** - Ikon data akan berwarna **abu-abu** jika status pengajuan **belum selesai**, proses `Ubah` dan `Hapus` masih bisa dilakukan, - Ikon data akan berwarna **hijau** jika status pengajuan sudah **selesai**, proses `Ubah` dan `Hapus` sudah tidak dapat dilakukan


<kbd>
  <img :src="$withBase('/assets/img/android/konsumsi_screen.jpeg')" alt="TampilanKonsumsiScreen" width="380">
</kbd>

### Tambah Pengajuan Konsumsi

Tekan tombol `+` pada bagian kanan layar, akan tampil layar tambah data pengajuan seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/android/konsumsi_add_01.png')" alt="TampilanKonsumsiAdd01">
</kbd>
<kbd>
  <img :src="$withBase('/assets/img/android/konsumsi_add_02.png')" alt="TampilanKonsumsiAdd02">
</kbd>


Field Label                     | Isian 
--  | - |
`Nama Kegiatan` | Isikan dengan judul kegiatan  <Badge text="wajib diisikan" type="error" />
`Tanggal_Kegiatan` | Tanggal mulai kegiatan harus 1 hari setelah diajukan <Badge text="wajib diisikan" type="error" />
`Tanggal Selesai`    | Tanggal selesai kegiatan harus sama dengan tanggal mulai kegiatan atau lebih besar <Badge text="wajib diisikan" type="error" />
`Lokasi/Ruangan`    | Masukan nama ruangan tempat kegiatan rapat <Badge text="wajib diisikan" type="error" />
`Jumlah Peserta`    | Masukan Angka jumlah peserta yang akan mengikuti kegiatan rapat
`Konsumsi Rapat`    | _Checklist_ jenis konsumsi yang diajukan, snack dan/atau makan siang
`Pengguna Rapat`    | Tekan pilihan yang ada, pilihan pengguna bisa lebih dari 1 pilihan
![Save](https://img.shields.io/badge/simpan-orange?&logo=data:image/svg%2bxml;base64,PHN2ZyBpZD0iQ2FwYV8xIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIuMDA3IDUxMi4wMDciIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgNTEyLjAwNyA1MTIuMDA3IiB3aWR0aD0iNTEyIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnPjxwYXRoIGQ9Im0xNDIgMTQzLjAwM2gxMzl2LTE0M2MtNDkuMzc5IDAtMTA3LjY3NCAwLTE1NCAwdjEyOGMwIDguMjcxIDYuNzI5IDE1IDE1IDE1eiIvPjxwYXRoIGQ9Im0zNDUgMTI4LjAwM3YtMTI4Yy05Ljk3IDAtMjEuNDYxIDAtMzQgMHYxNDNoMTljOC4yNzEgMCAxNS02LjcyOSAxNS0xNXoiLz48cGF0aCBkPSJtMTI3IDUxMi4wMDNoMjE4di0xNjFoLTIxOHoiLz48cGF0aCBkPSJtNTExLjkyNyAxMjYuNTM3Yy0uMjc5LTIuODI4LTEuMzgtNS42NjctMy4zMTUtOC4wMjctLjc0Ny0uOTEzIDYuODkzIDYuNzg2LTExNC4wMDYtMTE0LjExMy0yLjg4Mi0yLjg4Mi02Ljc5NC00LjM5Ni0xMC42MTItNC4zOTQtLjc4OSAwLTMuOTI1IDAtOC45OTUgMHYxMjhjMCAyNC44MTMtMjAuMTg3IDQ1LTQ1IDQ1LTE0LjAyOCAwLTE4Ni4wNjQgMC0xODggMC0yNC44MTMgMC00NS0yMC4xODctNDUtNDV2LTEyOGMtMjkuNTg5IDAtNDkuODIgMC01MiAwLTI0LjgxMyAwLTQ1IDIwLjE4Ny00NSA0NXY0MjJjMCAyNC44MTMgMjAuMTg3IDQ1IDQ1IDQ1aDUyYzAtMTAuODE1IDAtMjAxLjc5NyAwLTIxMCAwLTI0LjgxMyAyMC4xODctNDUgNDUtNDVoMTg4YzI0LjgxMyAwIDQ1IDIwLjE4NyA0NSA0NXYyMTBoOTJjMjQuODEzIDAgNDUtMjAuMTg3IDQ1LTQ1IC4wMDEtMzY0LjE4Ni4wNDEtMzM5LjMxNi0uMDcyLTM0MC40NjZ6Ii8+PHBhdGggZD0ibTMzMCAyODcuMDAzaC0xODhjLTguMjcxIDAtMTUgNi43MjktMTUgMTV2MTloMjE4di0xOWMwLTguMjcxLTYuNzI5LTE1LTE1LTE1eiIvPjwvZz48L3N2Zz4=) | Untuk menyimpan data, tekan tombol simpan pada bagian atas kanan layar

### View Data Pengajuan Konsumsi

Pada layar Konsumsi Rapat, pada data/record tekan tombol dengan ikon berbentuk mata 

<kbd>
  <img :src="$withBase('/assets/img/android/konsumsi_view.jpeg')" alt="TampilanKonsumsiView" width="380">
</kbd>

### Ubah Pengajuan Konsumsi

Pada layar Konsumsi Rapat, swipe ke kiri data yang akan kita ubah isiannya 

<kbd>
  <img :src="$withBase('/assets/img/android/konsumsi_swipekiri.jpeg')" alt="TampilanKonsumsiSwipeKiri" width="380">
</kbd>

Lalu tekan tombol `Ubah`, tombol `Ubah` tidak akan berfungsi jika status pengajuan sudah selesai.

### Tampilkan Laporan Konsumsi

Pada layar Konsumsi Rapat, swipe ke kiri data yang akan kita tampilkan data laporannya 

<kbd>
  <img :src="$withBase('/assets/img/android/konsumsi_swipekiri.jpeg')" alt="TampilanKonsumsiSwipeKiri" width="380">
</kbd>

Lalu tekan tombol `Tampilkan`, tombol `Tampilkan` tidak akan berfungsi jika status pengajuan belum selesai.

<kbd>
  <img :src="$withBase('/assets/img/android/konsumsi_lampiran.jpeg')" alt="TampilanKonsumsiLampiran" width="380">
</kbd>

### Unggah Ulang Laporan Konsumsi

Pada layar Konsumsi Rapat, swipe ke kanan data yang akan kita tampilkan data laporannya 

<kbd>
  <img :src="$withBase('/assets/img/android/konsumsi_swipekanan.jpeg')" alt="TampilanKonsumsiSwipeKanan" width="380">
</kbd>

Lalu tekan tombol `Unggah Ulang`, tombol `Unggah Ulang` tidak akan berfungsi jika status pengajuan belum selesai.

<kbd>
  <img :src="$withBase('/assets/img/android/konsumsi_unggah.jpeg')" alt="TampilanKonsumsiUnggahLampiran" width="380">
</kbd>

Pada halaman `Pilih dan Unggah File`, tekan tombol `AMBIL DOKUMEN`

<kbd>
  <img :src="$withBase('/assets/img/android/konsumsi_pilihunggah.jpeg')" alt="TampilanKonsumsiPilihUnggahLampiran" width="380">
</kbd>

---

## Sprin/Penugasan

Surat Perintah adalah sebuah dokumen yang dibuat untuk menugaskan pegawai untuk menjalankan tugas-tugas tertentu, pada aplikasi ini Surat Perintah/SPRIN ditampilkan hanya untuk pegawai yang bersangkutan saja, dan setiap pegawai akan memiliki data Sprin yang berbeda atau mungkin sama jika mereka ditugaskan dalam surat yang sama.
Menu Sprin ini tidak memiliki fitur input data, karena sifatnya hanya menerima saja. sedangkan untuk laporan hasil kegiatan akan diproses pada menu `Penomoran Laporan`

<kbd>
  <img :src="$withBase('/assets/img/android/sprin_screen.png')" alt="TampilanSprinScreen">
</kbd>

Untuk melihat Klasifikasi, Kategori dan Lokasi dapat menekan tombol &#8595; di posisi bawah kartu data Sprin

<kbd>
  <img :src="$withBase('/assets/img/android/sprin_screen_carddetail.png')" alt="TampilanSprinScreenCardDetail">
</kbd>


### Tampilkan Detil Sprin

Pada layar Sprin, swipe ke kiri data yang akan kita lihat/view detailnya

<kbd>
  <img :src="$withBase('/assets/img/android/sprin_screen_cardmenu.png')" alt="TampilanSprinScreenCardMenu">
</kbd>

Lalu tekan tombol `Detil`,

<kbd>
  <img :src="$withBase('/assets/img/android/sprin_detail.png')" alt="TampilanSprinDetail">
</kbd>

### Tampilkan Lampiran Sprin

Pada layar Sprin, swipe ke kiri data yang akan kita lihat/view detailnya

<kbd>
  <img :src="$withBase('/assets/img/android/sprin_screen_cardmenu.png')" alt="TampilanSprinScreenCardMenu">
</kbd>

Lalu tekan tombol `Tampilkan`,

<kbd>
  <img :src="$withBase('/assets/img/android/sprin_pdf.png')" alt="TampilanSprinPdf">
</kbd>

---

## Perjalanan Dinas (SPD)

Laporan perjalanan Dinas sama halnya dengan Penomoran Laporan, pada saat Sprin di inputkan dan pilihan SPD di ceklist, isian laporan perjalanan dinas akan terdapat pilihan Sprin yang sudah di input tersebut didalan inputan data laporan perjalanan dinas. Terdapat 4 bagian SPD dalam modul ini, sebagai berikut :
- **SPD Jalan**, merupakan SPD pengajuan dan belum direalisasikan serta masih mengalami kemungkinan perubahan data penerima Sprin, tetapi tetap harus di data dalam Laporan Perjalanan Dinas
- **SPD Rampung**, Merupakan SPD yang sudah direalisasikan dan memiliki status sudah _Fix_ dan tidak akan mengalami perubahan data penerima berdasarkan data yang dilaporkan.
- **SPD Diklat**, Merupakan SPD yang dikelompokan hanya perjalanan dinas untuk pendidikan dan pelatihan saja, tanpa memiliki pengeluaran untuk Penginapan dan Transportasi.
- **SPD Transport**, Merupakan SPD yang dikelompokkan hanya perjalanan dinas yang jenis pengeluarannya hanya Tansportasi saja.

> **_Catatan:_** Pada aplikasi Performa Mobile, seluruh data akan tampil jika status laporan sudah selesai

<kbd>
  <img :src="$withBase('/assets/img/android/spd_screen.png')" alt="TampilanSpdScreen">
</kbd>

Jika belum memiliki data, tampilan layar seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/android/spd_screen_nodata.png')" alt="TampilanSpdScreenNodata">
</kbd>

### Tampilkan Detil SPD

Bentuk tampilan layar seluruh SPD sama antara SPD Jalan, Rampung, Diklat dan Transport, Pada layar Sprin, swipe ke kiri data yang akan kita lihat/view detailnya

<kbd>
  <img :src="$withBase('/assets/img/android/spd_jalan_screen_cardmenu.png')" alt="TampilanSpdJalanScreenCardMenu">
</kbd>

Lalu tekan tombol `Detil`,

<kbd>
  <img :src="$withBase('/assets/img/android/spd_jalan_detail.png')" alt="TampilanSpdJalanDetail">
</kbd>

### Tampilkan Lampiran SPD

Pada layar Sprin, swipe ke kiri data yang akan kita lihat/view detailnya

<kbd>
  <img :src="$withBase('/assets/img/android/spd_jalan_screen_cardmenu.png')" alt="TampilanSpdJalanScreenCardMenu">
</kbd>

Lalu tekan tombol `Tampilkan`,

<kbd>
  <img :src="$withBase('/assets/img/android/spd_jalan_pdf.png')" alt="TampilanSpdJalanPdf">
</kbd>

---

## Penomoran Laporan

Penomoran Laporan berfungsi untuk mendapatkan terlebih dahulu Nomor Laporan sehingga pada saat laporan tersebut akan di unggah/upload Nomor sudah tersedia dan sesuai dengan tanggal kegiatan di dalam surat perintah (Sprin)

<kbd>
  <img :src="$withBase('/assets/img/android/penomoran_screen.png')" alt="TampilanPenomoranScreen">
</kbd>

Untuk melihat Keterangan Laporan,  Judul Sprin dan Nomor Sprin serta Tanggal Sprin dapat menekan tombol &#8595; di posisi bawah kartu data Penomoran Laporan

<kbd>
  <img :src="$withBase('/assets/img/android/penomoran_screen_carddetail.png')" alt="TampilanPenomoranScreenCardDetail">
</kbd>

### Tampilkan Detil Penomoran

Pada layar Penomoran Laporan, swipe ke kiri data yang akan kita lihat/view detailnya

<kbd>
  <img :src="$withBase('/assets/img/android/penomoran_screen_cardmenu.png')" alt="TampilanPenomoranScreenCardMenu">
</kbd>

Lalu tekan tombol `Detil`,

<kbd>
  <img :src="$withBase('/assets/img/android/penomoran_detail.png')" alt="TampilanPenomoranDetail">
</kbd>

### Tampilkan Lampiran Laporan

Pada layar Sprin, swipe ke kiri data yang akan kita lihat/view detailnya

<kbd>
  <img :src="$withBase('/assets/img/android/penomoran_screen_cardmenu.png')" alt="TampilanPenomoranScreenCardMenu">
</kbd>

Lalu tekan tombol `Tampilkan`,

<kbd>
  <img :src="$withBase('/assets/img/android/penomoran_pdf.png')" alt="TampilanPenomoranPdf">
</kbd>

---

## Nota Dinas (ND) Diklat

Nota Dinas Pendidikan dan Pelatihan merupakan proses mengikuti Pendidikan dan Pelatihan di lingkungan Inspektorat Utama BNN.
- Diawali dengan proses pemberitahuan untuk mengikuti Diklat dari pihak penyelenggara, dengan melakukan pengisian data kriteria serta persyaratan untuk mengikuti pelatihan pada _menu_ **ND Permintaan** 
- Kemnudian Sub Satker di Inspektorat Utama yang akan mengajukan pegawai/anggota untuk mengikuti Diklat,  dengan melakukan proses pengisian data pengajuan pegawai serta menyesuaikan kriteria dan persyaratan yang dipersyaratkan untuk mengikuti Diklat pada _menu_ **ND Pengajuan**,
- Setelah proses diatas, operator / admin akan melakukan proses Verifikasi Data, jika terdapat kekurangan data Operator akan mengirimkan Notifikasi / pemberitahuan mengenai Data/Dokumen yang belum lengkap. pada _menu_ **ND Pengajuan (Verifikasi)** 

> **_Catatan:_** Seluruh proses di atas dilakukan pada Aplikasi Performa Desktop, sedang aplikasi Performa Mobile, seluruh data akan tampil jika status Pengajuan sudah selesai

<kbd>
  <img :src="$withBase('/assets/img/android/ND_screen.png')" alt="TampilanNDScreen">
</kbd>