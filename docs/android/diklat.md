---
title: Diklat
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# PENDIDIKAN DAN PELATIHAN (DIKLAT)
[[toc]]
Halaman ini berisikan Data-data personal pegawai yang berhubungan dengan Pendidikan dan Pelatihan yang telah diikuti, agar mempermudah dalam memenuhi persyaratan untuk mengikuti Pendidikan dan Pelatihan berikut, yang dibutuhkan pada kelengkapan pengajuan Pendidikan dan Pelatihan (Diklat) di `Nota Dinas Pengajuan Diklat`, menu Diklat ini dibagi antara lain :

## Menu Diklat

- **Diklat Struktural**

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_screen_struktur.png')" alt="TampilanDiklatStruktur">
</kbd>

- **Diklat Penjenjangan**

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_screen_jenjang.png')" alt="TampilanDiklatPenjenjangan">
</kbd>

- **Diklat Teknis**, 

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_screen_teknis.png')" alt="TampilanDiklatTeknis">
</kbd>

- **Diklat NonFormal**

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_screen_nonformal.png')" alt="TampilanDiklatNonformal">
</kbd>

## Tambah Diklat

Tekan tombol `+` pada bagian kanan layar, akan tampil layar tambah data pengajuan seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_add.png')" alt="TampilanDiklatAdd">
</kbd>

Gambar berikut menampilkan pilihan Jenis Diklat yang akan diikuti
<kbd>
  <img :src="$withBase('/assets/img/android/diklat_add_pilihdiklat.jpeg')" alt="TampilanDiklatAddPilih" width="380">
</kbd>


Field Label                     | Isian 
--  | - |
`Pilih Diklat` | Pilih terlebih Jenis Diklat  ![Wajib Pilih !](https://img.shields.io/badge/Wajib-Pilih-1abc9c.svg)
`Judul Diklat` | Isikan Judul Diklat yang telah diikuti <Badge text="wajib diisikan" type="error" />
`Tahun Diklat`    | Isikan dengan Tahun saat mengikuti Diklat <Badge text="wajib diisikan" type="error" />
`Penyelenggara_Diklat`    | Isikan Instansi atau Lembaga penyelenggara Diklat <Badge text="wajib diisikan" type="error" />
`Nomor Ijasah`    | Isikan dengan Nomor Ijasah atau Sertifikat yang telah diterima
`Tanggal Ijasah`    | Isikan tanggal sesuai dengan data tanggal pada Ijasah atau Sertifikat
![Save](https://img.shields.io/badge/simpan-orange?&logo=data:image/svg%2bxml;base64,PHN2ZyBpZD0iQ2FwYV8xIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIuMDA3IDUxMi4wMDciIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgNTEyLjAwNyA1MTIuMDA3IiB3aWR0aD0iNTEyIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnPjxwYXRoIGQ9Im0xNDIgMTQzLjAwM2gxMzl2LTE0M2MtNDkuMzc5IDAtMTA3LjY3NCAwLTE1NCAwdjEyOGMwIDguMjcxIDYuNzI5IDE1IDE1IDE1eiIvPjxwYXRoIGQ9Im0zNDUgMTI4LjAwM3YtMTI4Yy05Ljk3IDAtMjEuNDYxIDAtMzQgMHYxNDNoMTljOC4yNzEgMCAxNS02LjcyOSAxNS0xNXoiLz48cGF0aCBkPSJtMTI3IDUxMi4wMDNoMjE4di0xNjFoLTIxOHoiLz48cGF0aCBkPSJtNTExLjkyNyAxMjYuNTM3Yy0uMjc5LTIuODI4LTEuMzgtNS42NjctMy4zMTUtOC4wMjctLjc0Ny0uOTEzIDYuODkzIDYuNzg2LTExNC4wMDYtMTE0LjExMy0yLjg4Mi0yLjg4Mi02Ljc5NC00LjM5Ni0xMC42MTItNC4zOTQtLjc4OSAwLTMuOTI1IDAtOC45OTUgMHYxMjhjMCAyNC44MTMtMjAuMTg3IDQ1LTQ1IDQ1LTE0LjAyOCAwLTE4Ni4wNjQgMC0xODggMC0yNC44MTMgMC00NS0yMC4xODctNDUtNDV2LTEyOGMtMjkuNTg5IDAtNDkuODIgMC01MiAwLTI0LjgxMyAwLTQ1IDIwLjE4Ny00NSA0NXY0MjJjMCAyNC44MTMgMjAuMTg3IDQ1IDQ1IDQ1aDUyYzAtMTAuODE1IDAtMjAxLjc5NyAwLTIxMCAwLTI0LjgxMyAyMC4xODctNDUgNDUtNDVoMTg4YzI0LjgxMyAwIDQ1IDIwLjE4NyA0NSA0NXYyMTBoOTJjMjQuODEzIDAgNDUtMjAuMTg3IDQ1LTQ1IC4wMDEtMzY0LjE4Ni4wNDEtMzM5LjMxNi0uMDcyLTM0MC40NjZ6Ii8+PHBhdGggZD0ibTMzMCAyODcuMDAzaC0xODhjLTguMjcxIDAtMTUgNi43MjktMTUgMTV2MTloMjE4di0xOWMwLTguMjcxLTYuNzI5LTE1LTE1LTE1eiIvPjwvZz48L3N2Zz4=) | Untuk menyimpan data, tekan tombol simpan pada bagian atas kanan layar

## Ubah Diklat

Swipe ke kiri pada data yang akan di ubah

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_swipekiri.jpeg')" alt="TampilanSwipeKiri" width="380">
</kbd>

Lalu tekan tombol `Ubah`

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_ubah.jpeg')" alt="TampilanDiklatUbah" width="380">
</kbd>

## Tampilkan Ijasah/Sertifikat

Swipe ke kiri pada data yang akan ditampilkan Ijasah/Sertifikat

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_swipekiri.jpeg')" alt="TampilanSwipeKiri" width="380">
</kbd>

Lalu tekan tombol `Tampilkan`

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_pdf0.jpeg')" alt="TampilanDiklatLampiran" width="380">
</kbd>

## Unggah Ulang Ijasah/Sertifikat

Swipe ke kanan pada data yang akan di unggah ulang Ijasah/Sertifikat

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_swipekanan.jpeg')" alt="TampilanSwipeKanan" width="380">
</kbd>

Lalu tekan tombol `Unggah Ulang`

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_unggah.jpeg')" alt="TampilanDiklatUnggah" width="380">
</kbd>

Pada halaman `Pilih dan Unggah File`, tekan tombol `AMBIL IJASAH`

<kbd>
  <img :src="$withBase('/assets/img/android/konsumsi_pilihunggah.jpeg')" alt="TampilanKonsumsiPilihUnggahLampiran" width="380">
</kbd>

## Hapus Data Diklat

Swipe ke kanan pada data yang akan di hapus

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_swipekanan.jpeg')" alt="TampilanSwipeKanan" width="380">
</kbd>

Lalu tekan tombol `Hapus`

<kbd>
  <img :src="$withBase('/assets/img/android/diklat_hapus.jpeg')" alt="TampilanDiklatHapus" width="380">
</kbd>