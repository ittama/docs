---
title: Layanan Mandiri
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# LAYANAN MANDIRI

[[toc]]

Layanan Mandiri merupakan Aplikasi Mobile yang dapat digunakan oleh seluruh pegawai untuk menampilkan Dokumen-dokumen personal seperti riwayat Cuti, Riwayat Jabatan (Mutasi), Riwayat Pangkat serta Riwayat Gaji Berkala, dan dapat juga mengajukan terkait cuti, Konsumsi dan Ruang Rapat, Penomoran Laporan, Penjilidan Laporan, Perjalanan Dinas (SPD) dan Surat Perintah (Sprin)/Penugasan.

## Halaman Login

Aplikasi hanya dapat digunakan oleh pegawai Inspektorat Utama BNN dengan menggunakan Nomor Pegawai (NIP/NRP)

<kbd>
  <img :src="$withBase('/assets/img/android/login.png')" alt="TampilanLogin">
</kbd>

### Nomor Pegawai

Nomor pegawai harus terdiri minimal 8 karakter (NRP) atau maksimal 18 karakter (NIP), bentuk keyboard hanya dalam bentuk angka.

<kbd>
  <img :src="$withBase('/assets/img/android/login_nrp.png')" alt="TampilanLoginNrp">
</kbd>

### Password

untuk password harus minimal 5 karakter, untuk password default masukan 6 karakter password yang telah diinformasikan

<kbd>
  <img :src="$withBase('/assets/img/android/login_password.png')" alt="TampilanLoginPassword">
</kbd>
