---
title: Kesejahteraan Pegawai
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# KESEJAHTERAAN PEGAWAI

[[toc]]

kesejahteraan pegawai disini berisikan data-data personal pegawai yang lingkupnya mulai dari Riwayat Jabatan, Pangkat, Kenaikan Gaji Berkala dan Riwayat Cuti bertujuan untuk meningkatkan kenyamanan serta produktivitas pegawai. Dalam modul ini lebih fokus pada perekaman data pegawai terkait : 
- Riwayat Jabatan (Mutasi), 
- Riwayat Pangkat,
- Riwayat Kenaikan Gaji Berkala, dan
- Riwayat Cuti

## Riwayat Jabatan (Mutasi)

Berisikan rekaman data personal pegawai terkait dengan riwayat jabatan/mutasi, hal ini bertujuan agar semua data yang tersimpan dapat selalu terupdate sehingga ke depan dapat dengan mudah untuk disinkronkan dengan data kepegawaian.

<kbd>
  <img :src="$withBase('/assets/img/android/mutasi_screen.png')" alt="TampilanMutasi">
</kbd>

## Tambah Riwayat Jabatan

Tekan tombol `+` pada bagian kanan layar, akan tampil layar tambah data Riwayat seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/android/mutasi_add.png')" alt="TampilanMutasiAdd">
</kbd>

Gambar berikut menampilkan pilihan Jabatan
<kbd>
  <img :src="$withBase('/assets/img/android/mutasi_add_pilihjabatan.png')" alt="TampilanMutasiAddPilih">
</kbd>


Field Label                     | Isian 
--  | - |
`Pilih_Jabatan` | Pilih terlebih dahulu Jabatan  ![Wajib Pilih !](https://img.shields.io/badge/Wajib-Pilih-1abc9c.svg)
`TMT` | Isikan dengan tanggal mulai pada jabatan tersebut sesuaikan dengan TMT (Terhitung Mulai Tanggal) pada dokumen lampiran
`Nomor SK`    | Isikan nomor Surat Keputusan <Badge text="wajib diisikan" type="error" />
`Tanggal SK`    | Isikan dengan tanggal Surat Keputusan
![Save](https://img.shields.io/badge/simpan-orange?&logo=data:image/svg%2bxml;base64,PHN2ZyBpZD0iQ2FwYV8xIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIuMDA3IDUxMi4wMDciIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgNTEyLjAwNyA1MTIuMDA3IiB3aWR0aD0iNTEyIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnPjxwYXRoIGQ9Im0xNDIgMTQzLjAwM2gxMzl2LTE0M2MtNDkuMzc5IDAtMTA3LjY3NCAwLTE1NCAwdjEyOGMwIDguMjcxIDYuNzI5IDE1IDE1IDE1eiIvPjxwYXRoIGQ9Im0zNDUgMTI4LjAwM3YtMTI4Yy05Ljk3IDAtMjEuNDYxIDAtMzQgMHYxNDNoMTljOC4yNzEgMCAxNS02LjcyOSAxNS0xNXoiLz48cGF0aCBkPSJtMTI3IDUxMi4wMDNoMjE4di0xNjFoLTIxOHoiLz48cGF0aCBkPSJtNTExLjkyNyAxMjYuNTM3Yy0uMjc5LTIuODI4LTEuMzgtNS42NjctMy4zMTUtOC4wMjctLjc0Ny0uOTEzIDYuODkzIDYuNzg2LTExNC4wMDYtMTE0LjExMy0yLjg4Mi0yLjg4Mi02Ljc5NC00LjM5Ni0xMC42MTItNC4zOTQtLjc4OSAwLTMuOTI1IDAtOC45OTUgMHYxMjhjMCAyNC44MTMtMjAuMTg3IDQ1LTQ1IDQ1LTE0LjAyOCAwLTE4Ni4wNjQgMC0xODggMC0yNC44MTMgMC00NS0yMC4xODctNDUtNDV2LTEyOGMtMjkuNTg5IDAtNDkuODIgMC01MiAwLTI0LjgxMyAwLTQ1IDIwLjE4Ny00NSA0NXY0MjJjMCAyNC44MTMgMjAuMTg3IDQ1IDQ1IDQ1aDUyYzAtMTAuODE1IDAtMjAxLjc5NyAwLTIxMCAwLTI0LjgxMyAyMC4xODctNDUgNDUtNDVoMTg4YzI0LjgxMyAwIDQ1IDIwLjE4NyA0NSA0NXYyMTBoOTJjMjQuODEzIDAgNDUtMjAuMTg3IDQ1LTQ1IC4wMDEtMzY0LjE4Ni4wNDEtMzM5LjMxNi0uMDcyLTM0MC40NjZ6Ii8+PHBhdGggZD0ibTMzMCAyODcuMDAzaC0xODhjLTguMjcxIDAtMTUgNi43MjktMTUgMTV2MTloMjE4di0xOWMwLTguMjcxLTYuNzI5LTE1LTE1LTE1eiIvPjwvZz48L3N2Zz4=) | Untuk menyimpan data, tekan tombol simpan pada bagian atas kanan layar

## Riwayat Pangkat

Berisikan rekaman data personal pegawai terkait dengan riwayat Pangkat, hal ini bertujuan agar semua data yang tersimpan dapat selalu terupdate sehingga ke depan dapat dengan mudah untuk disinkronkan dengan data kepegawaian.

<kbd>
  <img :src="$withBase('/assets/img/android/pangkat_screen.png')" alt="TampilanPangkat">
</kbd>

## Tambah Riwayat Pangkat

Tekan tombol `+` pada bagian kanan layar, akan tampil layar tambah data Riwayat seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/android/pangkat_add.png')" alt="TampilanPangkatAdd">
</kbd>

Gambar berikut menampilkan pilihan Jabatan
<kbd>
  <img :src="$withBase('/assets/img/android/pangkat_add_pilihpangkat.png')" alt="TampilanPangkatAddPilih">
</kbd>


Field Label                     | Isian 
--  | - |
`Pilih_Pangkat` | Pilih terlebih dahulu Pangkat  ![Wajib Pilih !](https://img.shields.io/badge/Wajib-Pilih-1abc9c.svg)
`TMT` | Isikan dengan tanggal mulai pada Pangkat tersebut sesuaikan dengan TMT (Terhitung Mulai Tanggal) pada dokumen lampiran
`Nomor SK`    | Isikan nomor Surat Keputusan <Badge text="wajib diisikan" type="error" />
`Tanggal SK`    | Isikan dengan tanggal Surat Keputusan
![Save](https://img.shields.io/badge/simpan-orange?&logo=data:image/svg%2bxml;base64,PHN2ZyBpZD0iQ2FwYV8xIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIuMDA3IDUxMi4wMDciIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgNTEyLjAwNyA1MTIuMDA3IiB3aWR0aD0iNTEyIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnPjxwYXRoIGQ9Im0xNDIgMTQzLjAwM2gxMzl2LTE0M2MtNDkuMzc5IDAtMTA3LjY3NCAwLTE1NCAwdjEyOGMwIDguMjcxIDYuNzI5IDE1IDE1IDE1eiIvPjxwYXRoIGQ9Im0zNDUgMTI4LjAwM3YtMTI4Yy05Ljk3IDAtMjEuNDYxIDAtMzQgMHYxNDNoMTljOC4yNzEgMCAxNS02LjcyOSAxNS0xNXoiLz48cGF0aCBkPSJtMTI3IDUxMi4wMDNoMjE4di0xNjFoLTIxOHoiLz48cGF0aCBkPSJtNTExLjkyNyAxMjYuNTM3Yy0uMjc5LTIuODI4LTEuMzgtNS42NjctMy4zMTUtOC4wMjctLjc0Ny0uOTEzIDYuODkzIDYuNzg2LTExNC4wMDYtMTE0LjExMy0yLjg4Mi0yLjg4Mi02Ljc5NC00LjM5Ni0xMC42MTItNC4zOTQtLjc4OSAwLTMuOTI1IDAtOC45OTUgMHYxMjhjMCAyNC44MTMtMjAuMTg3IDQ1LTQ1IDQ1LTE0LjAyOCAwLTE4Ni4wNjQgMC0xODggMC0yNC44MTMgMC00NS0yMC4xODctNDUtNDV2LTEyOGMtMjkuNTg5IDAtNDkuODIgMC01MiAwLTI0LjgxMyAwLTQ1IDIwLjE4Ny00NSA0NXY0MjJjMCAyNC44MTMgMjAuMTg3IDQ1IDQ1IDQ1aDUyYzAtMTAuODE1IDAtMjAxLjc5NyAwLTIxMCAwLTI0LjgxMyAyMC4xODctNDUgNDUtNDVoMTg4YzI0LjgxMyAwIDQ1IDIwLjE4NyA0NSA0NXYyMTBoOTJjMjQuODEzIDAgNDUtMjAuMTg3IDQ1LTQ1IC4wMDEtMzY0LjE4Ni4wNDEtMzM5LjMxNi0uMDcyLTM0MC40NjZ6Ii8+PHBhdGggZD0ibTMzMCAyODcuMDAzaC0xODhjLTguMjcxIDAtMTUgNi43MjktMTUgMTV2MTloMjE4di0xOWMwLTguMjcxLTYuNzI5LTE1LTE1LTE1eiIvPjwvZz48L3N2Zz4=) | Untuk menyimpan data, tekan tombol simpan pada bagian atas kanan layar

## Riwayat KGB

Berisikan rekaman data personal pegawai terkait dengan riwayat Kenaikan Gaji Berkala (KGB), hal ini bertujuan agar semua data yang tersimpan dapat selalu terupdate sehingga ke depan dapat dengan mudah untuk disinkronkan dengan data kepegawaian.

<kbd>
  <img :src="$withBase('/assets/img/android/kgb_screen.png')" alt="TampilanKGB">
</kbd>

## Tambah Riwayat KGB

Tekan tombol `+` pada bagian kanan layar, akan tampil layar tambah data Riwayat seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/android/kgb_add.png')" alt="TampilanKGBAdd">
</kbd>

Gambar berikut menampilkan pilihan Jabatan
<kbd>
  <img :src="$withBase('/assets/img/android/kgb_add_pilihgol.png')" alt="TampilanKGBAddPilih">
</kbd>


Field Label                     | Isian 
--  | - |
`Pilih_Golongan_Ruang` | Pilih terlebih dahulu Golongan dan Ruang Pegawai  ![Wajib Pilih !](https://img.shields.io/badge/Wajib-Pilih-1abc9c.svg)
`Masa Kerja` | Isikan dengan Angka tahun masa kerja <Badge text="wajib diisikan" type="error" />
`TMT` | Isikan dengan tanggal mulai pada KGB tersebut sesuaikan dengan TMT (Terhitung Mulai Tanggal) pada dokumen lampiran
`Nomor SK`    | Isikan nomor Surat Keputusan <Badge text="wajib diisikan" type="error" />
`Tanggal SK`    | Isikan dengan tanggal Surat Keputusan
![Save](https://img.shields.io/badge/simpan-orange?&logo=data:image/svg%2bxml;base64,PHN2ZyBpZD0iQ2FwYV8xIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIuMDA3IDUxMi4wMDciIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgNTEyLjAwNyA1MTIuMDA3IiB3aWR0aD0iNTEyIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnPjxwYXRoIGQ9Im0xNDIgMTQzLjAwM2gxMzl2LTE0M2MtNDkuMzc5IDAtMTA3LjY3NCAwLTE1NCAwdjEyOGMwIDguMjcxIDYuNzI5IDE1IDE1IDE1eiIvPjxwYXRoIGQ9Im0zNDUgMTI4LjAwM3YtMTI4Yy05Ljk3IDAtMjEuNDYxIDAtMzQgMHYxNDNoMTljOC4yNzEgMCAxNS02LjcyOSAxNS0xNXoiLz48cGF0aCBkPSJtMTI3IDUxMi4wMDNoMjE4di0xNjFoLTIxOHoiLz48cGF0aCBkPSJtNTExLjkyNyAxMjYuNTM3Yy0uMjc5LTIuODI4LTEuMzgtNS42NjctMy4zMTUtOC4wMjctLjc0Ny0uOTEzIDYuODkzIDYuNzg2LTExNC4wMDYtMTE0LjExMy0yLjg4Mi0yLjg4Mi02Ljc5NC00LjM5Ni0xMC42MTItNC4zOTQtLjc4OSAwLTMuOTI1IDAtOC45OTUgMHYxMjhjMCAyNC44MTMtMjAuMTg3IDQ1LTQ1IDQ1LTE0LjAyOCAwLTE4Ni4wNjQgMC0xODggMC0yNC44MTMgMC00NS0yMC4xODctNDUtNDV2LTEyOGMtMjkuNTg5IDAtNDkuODIgMC01MiAwLTI0LjgxMyAwLTQ1IDIwLjE4Ny00NSA0NXY0MjJjMCAyNC44MTMgMjAuMTg3IDQ1IDQ1IDQ1aDUyYzAtMTAuODE1IDAtMjAxLjc5NyAwLTIxMCAwLTI0LjgxMyAyMC4xODctNDUgNDUtNDVoMTg4YzI0LjgxMyAwIDQ1IDIwLjE4NyA0NSA0NXYyMTBoOTJjMjQuODEzIDAgNDUtMjAuMTg3IDQ1LTQ1IC4wMDEtMzY0LjE4Ni4wNDEtMzM5LjMxNi0uMDcyLTM0MC40NjZ6Ii8+PHBhdGggZD0ibTMzMCAyODcuMDAzaC0xODhjLTguMjcxIDAtMTUgNi43MjktMTUgMTV2MTloMjE4di0xOWMwLTguMjcxLTYuNzI5LTE1LTE1LTE1eiIvPjwvZz48L3N2Zz4=) | Untuk menyimpan data, tekan tombol simpan pada bagian atas kanan layar

## Riwayat Cuti

Berisikan rekaman data personal pegawai terkait dengan riwayat Cuti, hal ini bertujuan agar semua data yang tersimpan dapat selalu terupdate sehingga ke depan dapat dengan mudah untuk disinkronkan dengan data kepegawaian.

<kbd>
  <img :src="$withBase('/assets/img/android/cuti_screen.png')" alt="TampilanCuti">
</kbd>

## Tambah Riwayat Cuti

Tekan tombol `+` pada bagian kanan layar, akan tampil layar tambah data Riwayat seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/android/cuti_add.png')" alt="TampilanCutiAdd">
</kbd>

Gambar berikut menampilkan pilihan Jenis Cuti
<kbd>
  <img :src="$withBase('/assets/img/android/cuti_add_pilihjeniscuti.png')" alt="TampilanCutiAddPilih">
</kbd>


Field Label                     | Isian 
--  | - |
`Pilih_Jenis_Cuti` | Pilih terlebih dahulu Jenis Cuti ![Wajib Pilih !](https://img.shields.io/badge/Wajib-Pilih-1abc9c.svg)
`Hak Cuti` | Isikan dengan Angka jumlah hak cuti <Badge text="wajib diisikan" type="error" />
`Nomor SK`    | Isikan nomor Surat Keputusan <Badge text="wajib diisikan" type="error" />
`Tanggal SK`    | Isikan dengan tanggal Surat Keputusan
`Tanggal Mulai Cuti`    | Isikan dengan tanggal mulai cuti
`Tanggal_Selesai_Cuti`    | Isikan dengan tanggal berakhir cuti
![Save](https://img.shields.io/badge/simpan-orange?&logo=data:image/svg%2bxml;base64,PHN2ZyBpZD0iQ2FwYV8xIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIuMDA3IDUxMi4wMDciIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgNTEyLjAwNyA1MTIuMDA3IiB3aWR0aD0iNTEyIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnPjxwYXRoIGQ9Im0xNDIgMTQzLjAwM2gxMzl2LTE0M2MtNDkuMzc5IDAtMTA3LjY3NCAwLTE1NCAwdjEyOGMwIDguMjcxIDYuNzI5IDE1IDE1IDE1eiIvPjxwYXRoIGQ9Im0zNDUgMTI4LjAwM3YtMTI4Yy05Ljk3IDAtMjEuNDYxIDAtMzQgMHYxNDNoMTljOC4yNzEgMCAxNS02LjcyOSAxNS0xNXoiLz48cGF0aCBkPSJtMTI3IDUxMi4wMDNoMjE4di0xNjFoLTIxOHoiLz48cGF0aCBkPSJtNTExLjkyNyAxMjYuNTM3Yy0uMjc5LTIuODI4LTEuMzgtNS42NjctMy4zMTUtOC4wMjctLjc0Ny0uOTEzIDYuODkzIDYuNzg2LTExNC4wMDYtMTE0LjExMy0yLjg4Mi0yLjg4Mi02Ljc5NC00LjM5Ni0xMC42MTItNC4zOTQtLjc4OSAwLTMuOTI1IDAtOC45OTUgMHYxMjhjMCAyNC44MTMtMjAuMTg3IDQ1LTQ1IDQ1LTE0LjAyOCAwLTE4Ni4wNjQgMC0xODggMC0yNC44MTMgMC00NS0yMC4xODctNDUtNDV2LTEyOGMtMjkuNTg5IDAtNDkuODIgMC01MiAwLTI0LjgxMyAwLTQ1IDIwLjE4Ny00NSA0NXY0MjJjMCAyNC44MTMgMjAuMTg3IDQ1IDQ1IDQ1aDUyYzAtMTAuODE1IDAtMjAxLjc5NyAwLTIxMCAwLTI0LjgxMyAyMC4xODctNDUgNDUtNDVoMTg4YzI0LjgxMyAwIDQ1IDIwLjE4NyA0NSA0NXYyMTBoOTJjMjQuODEzIDAgNDUtMjAuMTg3IDQ1LTQ1IC4wMDEtMzY0LjE4Ni4wNDEtMzM5LjMxNi0uMDcyLTM0MC40NjZ6Ii8+PHBhdGggZD0ibTMzMCAyODcuMDAzaC0xODhjLTguMjcxIDAtMTUgNi43MjktMTUgMTV2MTloMjE4di0xOWMwLTguMjcxLTYuNzI5LTE1LTE1LTE1eiIvPjwvZz48L3N2Zz4=) | Untuk menyimpan data, tekan tombol simpan pada bagian atas kanan layar