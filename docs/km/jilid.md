---
title: Penjilidan Laporan
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# PENJILIDAN LAPORAN

Penjilidan Laporan adalah proses permohonan untuk melakukan penjilidan laporan berdasarkan penomoran laporan yang telah disiapkan pada saat input data Surat Perintah dilakukan. Informasi yang diisikan dalam form penjilidan laporan adalah jumlah laporan yang dibutuhkan, bentuk penjilidan dapat berupa Spiral, Hard cover atau Biasa, dan informasi waktu penyelesaian penjilidan. 