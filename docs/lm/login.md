---
title: Login
date: 2020-12-24
tags:
  - login
  - logout
  - menu
author: Torina
featuredimg: "https://images.unsplash.com/photo-1482876555840-f31c5ebbff1c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1351&q=80"
summary: For a moment she wondered how she had happened to wake so early.
---

# LOGIN DAN GROUP

## Login <Badge text="Semua"/>

Akses halaman aplikasi menggunakan browser ke alamat [Aplikasi Performa](http://ittama.personil.id), lalu masukkan data NIP/NRP kemudian Password.

<img :src="$withBase('/assets/img/lm/login_lm.png')" alt="TampilanLogin">

Seluruh data Pengguna dan Password untuk melakukan Login disiapkan oleh Administrator, karena tidak ada fitur mendaftarkan akun.

## Grup Pengguna <Badge text="Administrator"/>

Grup Pengguna
<kbd>
  <img :src="$withBase('/assets/img/lm/menu_group.png')" alt="TampilanMenuGroup">
</kbd>

### 1. Grup Administrator

Sebagai pengguna Administrator, memiliki otorisasi mengatur grup pengguna, menambahkan pengguna, menentukan menu-menu yang dapat di akses oleh pengguna, Administrator tidak memiliki akses untuk melakukan transaksi

### 2. Grup Operator

Sebagai pengguna Operator, memiliki otorisasi untuk melakukan transaksi hanya pada bagian yang di otorisasi saja, misal Operator untuk Perpustakaan Pegawai, hanya bertanggungjawab dan melakukan transaksi sebatas lingkup Perpustakaan Pegawai saja.

### 3. Grup Pengguna Personal

Pengguna Personal adalah pengguna Mobile dan hanya melalui aplikasi mobile saja (sementara ini hanya Android OS), yang hanya dapat di akses hanya oleh pegawai yang memiliki NIP atau NRP dan hanya pegawai Inspektorat Utama.

<kbd>
  <img :src="$withBase('/assets/img/lm/grid_group.png')" alt="TampilanGridGroup">
</kbd>

## Modifikasi Data Group
Pada layar/tampilan diatas, tekan tombol `Tambah` lalu akan muncul kolom untuk menambahkan data baru seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/lm/IsianGroup.png')" alt="TampilanIsianGroup">
</kbd>

Kemudian tekan tombol Update pada gambar diatas, untuk memilih otorisasi pada Menu dan Form pada Aplikasi, sesuaikan dengan melakukan :white_check_mark: ceklist pada layar sebelah kanan Grid pada Tampilan Group.

<kbd>
<img :src="$withBase('/assets/img/lm/grid_group3.png')" alt="TampilanOtoritasGroup">
</kbd>

Setelah menentukan akses untuk group Pengguna, tekan Tombol `Simpan`.
untuk melakukan perubahan data Groud silahkan menekan tombol `Ubah` atau tombol `Hapus` untuk menghapus data Group.