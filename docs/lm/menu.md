---
title: Menu
date: 2020-12-24
tags:
  - login
  - logout
  - menu
author: Torina
---

# MENU

## Menu Bar <Badge text="Semua"/>

Tampilan Menu Bar
<kbd>
    <img :src="$withBase('/assets/img/lm/view_menubar.png')" alt="TampilanViewMenubar">
</kbd>

- Tombol Refresh <img :src="$withBase('/assets/img/lm/menubar_refresh.png')" alt="TampilanMenubarRefresh"> adalah tombol yang berfungsi untuk me-_refresh_ ulang halaman
- Tombol Profile <img :src="$withBase('/assets/img/lm/menubar_profil.png')" alt="TampilanMenubarProfil"> adalah tombol untuk menampilkan profil dari pengguna, dan juga terdapat fitur untuk mengubah password dari pengguna
- Label Pengguna <img :src="$withBase('/assets/img/lm/menubar_label.png')" alt="TampilanMenubarLabel">, menginformasikan pengguna yang sedang login
- Tombol Exit/Logout <img :src="$withBase('/assets/img/lm/menubar_exit.png')" alt="TampilanMenubarExit"> jika sudah selesai menggunakan aplikasi dapat keluar aplikasi dengan menekan tombol ini




## Menu <Badge text="Administrator"/>

Menu Administrator meliputi seluruh menu yang ada didalam Aplikasi, karena administrator hanya memiliki kemampuan untuk pengaturan Otoritas pengguna terhadap Modul yang terdapat di dalam aplikasi dan tidak memiliki kemampuan untuk melakukan transaksi data.

<img :src="$withBase('/assets/img/lm/menu_admin.png')" alt="TampilanMenuAdmin">

## Menu Perpustakaan Pegawai <Badge text="Operator"/>

Menu Operator Perpustakaan Pegawai yang meliputi Menu Pendidikan dan Pelatihan, Riwayat Jabatan (Mutasi), Riwayat Pangkat, Riwayat Cuti dan Riwayat Kenaikan Gaji Berkala. Modul ini berfungsi untuk melakukan penyimpanan data-data riwayat seluruh pegawai Inspektorat Utama BNN, masih bersifat pendataan agar seluruh data pegawai dapat di akses terpusat, yang nantinya akan di sinkronisasi dengan data kepegawaian.

<img :src="$withBase('/assets/img/lm/menu_pustaka.png')" alt="TampilanMenuPustaka">

## Menu Pelayanan Pegawai <Badge text="Operator"/>

Menu Operator Pelayanan Pegawai meliputi Menu layanan pengajuan serta informasi untuk pegawai seperti Pengajuan ATK, Pemeliharaan dan Perawatan (Harwat), Penjilidan Laporan, Konsumsi Rapat, dan Pengajuan Cuti sampai dengan Informasi Surat Perintah/Penugasan, Perjalanan Dinas (SPD), Penomoran Laporan serta Nota Dinas Pendidikan dan Pelatihan. 

<img :src="$withBase('/assets/img/lm/menu_layan.png')" alt="TampilanMenuLayan">