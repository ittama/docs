# Perpustakaan Pegawai

[[toc]]

Perpustakaan pegawai merupakan kumpulan data pegawai berupa profil lengkap pegawai dan menyimpan dokumen-dokumen Surat Keputusan,  Riwayat Jabatan (Mutasi), Riwayat Pangkat, Riwayat Kenaikan Gaji Berkala, Riwayat cuti dan Sertifikat serta Ijasah Pendidikan dan Pelatihan (Diklat) setiap pegawai yang dapat dilihat dan ditampilkan sesuai kebutuhan. _menu_ [Profil](/nm/profil/).

<img :src="$withBase('/assets/img/nm/grid_pustaka.png')" alt="TampilanGridPegawai">

