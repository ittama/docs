---
title: Kesejahteraan Pegawai
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# KESEJAHTERAAN PEGAWAI

kesejahteraan pegawai adalah segala usaha yang dilakukan oleh sebuah instansi untuk meningkatkan kenyamanan serta produktivitas pegawai tanpa mengurangi upah. Dalam modul ini lebih fokus pada perekaman data pegawai terkait : 
- Riwayat Jabatan (Mutasi), 
- Riwayat Pangkat,
- Riwayat Kenaikan Gaji Berkala, dan
- Riwayat Cuti

<kbd>
    <img :src="$withBase('/assets/img/nm/view_sejahtera.png')" alt="TampilanViewKesejahteraan">
</kbd>

## Riwayat Jabatan

Menu Riwayat Jabatan (Mutasi) ini menampilkan rekaman data riwayat jabatan seluruh pegawai, penambahan data dapat dilakukan di aplikasi Desktop dan Mobile sehingga  seluruh data yang tersimpan dapat saling di konfirmasi.

<kbd>
    <img :src="$withBase('/assets/img/nm/view_jabatan.png')" alt="TampilanViewJabatan">
</kbd>

## Riwayat Pangkat

Menu Riwayat Pangkat ini menampilkan rekaman data riwayat pangkat seluruh pegawai, penambahan data dapat dilakukan di aplikasi Desktop dan Mobile sehingga seluruh data yang tersimpan dapat saling di konfirmasi.

<kbd>
    <img :src="$withBase('/assets/img/nm/view_pangkat.png')" alt="TampilanViewPangkat">
</kbd>

## Riwayat Kenaikan Gaji Berkala

Menu Riwayat Kenaikan Gaji Berkala (KGB) ini menampilkan rekaman data riwayat KGB seluruh pegawai, penambahan data dapat dilakukan di aplikasi Desktop dan Mobile sehingga  seluruh data yang tersimpan dapat saling di konfirmasi.

<kbd>
    <img :src="$withBase('/assets/img/nm/view_kgb.png')" alt="TampilanViewKGB">
</kbd>

## Riwayat Cuti

Menu Riwayat Cuti ini menampilkan rekaman data riwayat cuti seluruh pegawai, penambahan data dapat dilakukan di aplikasi Desktop dan Mobile sehingga  seluruh data yang tersimpan dapat saling di konfirmasi.

<kbd>
    <img :src="$withBase('/assets/img/nm/view_cuti.png')" alt="TampilanViewCuti">
</kbd>


## Tambah Data <Badge text="Jabatan, Pangkat, KGB dan Cuti" type="warning"/>
Pada layar/tampilan _Grid_ terletak di bagian kiri atas, tekan tombol `Tambah` lalu akan muncul kolom untuk menambahkan data baru seperti berikut :

### Tambah Jabatan

<kbd>
  <img :src="$withBase('/assets/img/nm/jabatan_add.png')" alt="TampilanJabatanTambah">
</kbd>

### Tambah Pangkat

<kbd>
  <img :src="$withBase('/assets/img/nm/pangkat_add.png')" alt="TampilanPangkatTambah">
</kbd>

### Tambah KGB

<kbd>
  <img :src="$withBase('/assets/img/nm/kgb_add.png')" alt="TampilanKGBTambah">
</kbd>

### Tambah Cuti

<kbd>
  <img :src="$withBase('/assets/img/nm/cuti_add.png')" alt="TampilanCutiTambah">
</kbd>

Lengkapi data yang akan ditambahkan, Kemudian tekan tombol `Update` pada gambar diatas, untuk mengkonfirmasi agar data tersimpan ke server.


## Ubah Data <Badge text="Jabatan, Pangkat, KGB dan Cuti" type="warning"/>

Pastikan pilih baris data yang akan di ubah pada layar _Grid_, lalu pada layar/tampilan _Grid_ terletak di bagian kiri atas, tekan tombol `Ubah` lalu akan muncul kolom ubah pada baris data yang sudah dipilih untuk melakukan perubahan data seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/jabatan_edit.png')" alt="TampilanJabatanUbah">
</kbd>

Secara umum tampilan pada saat mengubah data sama dengan penambahan data,lengkapi dan sesuaikan data yang akan di ubah, Kemudian tekan tombol `Update` pada gambar diatas, untuk mengkonfirmasi agar data tersimpan ke server.


## Unggah (Upload) Data <Badge text="Jabatan, Pangkat, KGB dan Cuti" type="warning"/>

Pastikan pilih baris data yang akan di tambahkan lampiran pada layar _Grid_, lalu pada layar/tampilan _Grid_  terletak di bagian kanan atas, tekan tombol `Upload` lalu akan muncul tampilan upload data yang akan dilampirkan pada untuk menambahkan data baru seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/diklat_upload.png')" alt="TampilanDiklatUpload">
</kbd>

Kemudian tekan tombol `Simpan` pada gambar diatas, untuk mengkonfirmasi agar data lampiran tersimpan ke server atau tekan tombol `Batal` untuk membatalkan penambahan lampiran.

## Hapus Data <Badge text="Jabatan, Pangkat, KGB dan Cuti" type="warning"/>

Pastikan pilih baris data yang akan di hapus pada layar _Grid_, lalu pada layar/tampilan _Grid_  terletak di bagian kanan atas, tekan tombol `Hapus` lalu akan muncul layar konfirmasi untuk menghapus data yang telah dipilih, seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/diklat_hapus.png')" alt="TampilanDiklatHapus">
</kbd>

Kemudian tekan tombol `Ya` pada gambar diatas, untuk mengkonfirmasi agar data terhapus dari server atau tekan tombol `Tidak` untuk membatalkan proses penghapusan data.