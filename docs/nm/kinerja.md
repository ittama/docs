---
title: Kinerja Pegawai
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# KINERJA PEGAWAI

Pada menu Kinerja ini, akan menampilkan dokumen-dokumen kegiatan yang dilakukan di lingkungan Inspektorat Utama BNN, seperti Surat-surat Perintah atau Penugasan, Kalender kegiatan, Laporan Kegiatan dan Rekapitulasi dari kegiatan-kegiatan tersebut.

## Surat Perintah (Sprin)/Penugasan

Menampilkan Dokumen-dokumen penugasan berdasarkan Klasifikasi Arsip

<kbd>
  <img :src="$withBase('/assets/img/nm/view_sprin.png')" alt="TampilanViewSprin">
</kbd>

## Timeline

Menampilkan Kalender kegiatan-kegiatan yang di rencanakan pada bulan atau tahun berjalan

### Table View

<kbd>
  <img :src="$withBase('/assets/img/nm/timeline_table.png')" alt="TampilanTableTimeline">
</kbd>

## Laporan Kegiatan

Menampilkan Dokumen-dokumen Laporan Kegiatan dari Surat Perintah/Penugasan yang ditampilkan berdasarkan Klasifikasi Arsip

<kbd>
  <img :src="$withBase('/assets/img/nm/laporan_giat.png')" alt="TampilanViewLapgiat">
</kbd>

## Rekapitulasi

### Rekapitulasi Kegiatan

<kbd>
  <img :src="$withBase('/assets/img/nm/rekap_giat.png')" alt="TampilanViewRekapgiat">
</kbd>

### Rekapitulasi Kompetensi

<kbd>
  <img :src="$withBase('/assets/img/nm/rekap_kompetensi.png')" alt="TampilanViewRekapKompeten">
</kbd>