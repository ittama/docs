---
title: Kompetensi Pegawai
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# KOMPETENSI PEGAWAI

Kompetensi kerja didefinisikan sebagai kemampuan pengetahuan dan keterampilan/kecakapan yang dimiliki oleh pegawai/aparatur yang relevan dengan pekerjaan, tugas apapun jabatannya. Dengan tujuan sebagai berikut :

- Membangun integritas moral, kejujuran, semangat dan motivasi nasionalisme dan kebangsaan, karakter kepribadian yang unggul dan bertanggung jawab, dan memperkuat profesionalisme serta kompetensi bidang.
- Meningkatkan pengetahuan, keahlian, keterampilan dan sikap para Pegawai Negeri Sipil (PNS) dalam melaksanakan tugas jabatan secara profesional dengan dilandasi oleh kepribadian dan etika PNS yang sesuai dengan kebutuhan instansi.
- Menciptakan aparatur sipil negara yang mampu berperan sebagai pembaharu dan perekat persatuan dan juga kesatuan bangsa.
- Memantapkan berbagai sikap dan semangat pengabdian yang berorientasi pada pelayanan, pengayoman dan pemberdayaan masyarakat.
- Menciptakan kesamaan visi dan dinamika pola pikir para aparatur sipil negara di dalam melaksanakan tugas pemerintahan umum dan pembangunan demi terwujudnya pemerintahan yang baik.

## Pendidikan dan Pelatihan Struktural <Badge text="Diklat"/>

Pendidikan dan Pelatihan Struktural atau dalam Jabatan dilaksanakan untuk mengembangkan pengetahuan, keterampilan, dan sikap PNS agar dapat melaksanakan tugas-tugas pemerintahan dan pembangunan dengan sebaik-baiknya. Di dalamnya termasuk Pendidikan dan Pelatihan Kepemimpinan, diklat ini dilakukan guna memberikan wawasan, pengetahuan, keahlian, ketrampilan, sikap serta perilaku dalam bidang kepemimpinan aparatur, yang bertujuan untuk mencapai persyaratan kompetensi kepemimpinan dalam jenjang jabatan struktural tertentu

<kbd>
    <img :src="$withBase('/assets/img/nm/view_struktural.png')" alt="TampilanViewStruktural">
</kbd>

## Pendidikan dan Pelatihan Penjenjangan <Badge text="Diklat"/>

Pendidikan dan Pelatihan Penjenjangan atau Fungsional adalah bentuk diklat yang dilakukan untuk memberikan bekal pengetahuan dan atau ketrampilan bagi para Pegawai Negeri Sipil yang sesuai dengan keahlian dan ketrampilan yang diperlukan dalam jabatan fungsional.

Diklat Fungsional merupakan jenis Diklat Pegawai Negeri Sipil yang dilaksanakan untuk mencapai persyaratan kompetensi sesuai dengan jenis dan jenjang jabatan fungsional masing-masing. Jenjang jabatan fungsional ini terdiri dari :

- Diklat fungsional keahlian yang merupkaan bentuk diklat yang memberikan pengetahuan dan keahlian fungsional tertentu dan terkait langsung dengan pelaksanaan tugas jabatan fungsional keahlian yang bersangkutan.
- Diklat fungsional ketrampilan yang merupakan bentuk diklat yang memberikan pengetahuan dan ketrampilan fungsional tertentu dan terkait langsung dengan pelaksanaan tugas jabatan fungsional keahlian yang bersangkutan.

<kbd>
    <img :src="$withBase('/assets/img/nm/view_jenjang.png')" alt="TampilanViewPenjenjangan">
</kbd>

## Pendidikan dan Pelatihan Teknis <Badge text="Diklat"/>

Pendidikan dan Pelatihan Teknis merupakan diklat yang dilakukan guna mencapai persyaratan kompetensi teknis yang diperlukan untuk pelaksanaan tugas para PNS. Kompetensi Teknis ini merupakan kemampuan PNS dalam bidang-bidang teknis tertentu yang digunakan demi pelaksanaan tugas masing-masing. Diklat teknis meliputi :

- Diklat teknis bidang umum/administrasi dan manajemen yang merupakan diklat yang memberikan ketrampilan dan/atau penguasaan pengetahuan dalam bidang pelayanan teknis yang sifatnya umum serta di bidang administrasi dan manajemen guna menunjang tugas pokok instansi yang bersangkutan.
- Diklat teknis substantif yang merupakan diklat yang memberikan ketrampilan dan/ atau penguasaan pengetahuan teknis terkait secara langsung dengan pelaksanaan tugas pokok instansi yang bersangkutan.

<kbd>
    <img :src="$withBase('/assets/img/nm/view_teknis.png')" alt="TampilanViewTeknis">
</kbd>

## Pendidikan dan Pelatihan Non-Formal <Badge text="Diklat"/>

Diklat Non Formal adalah diklat yang tidak wajib dilaksanakan oleh para Pegawai Negeri Sipil (PNS)

<kbd>
    <img :src="$withBase('/assets/img/nm/view_nonformal.png')" alt="TampilanViewNonformal">
</kbd>


## Tambah Data Diklat
Pada layar/tampilan _Grid_ Diklat terletak di bagian kiri atas, tekan tombol `Tambah` lalu akan muncul kolom untuk menambahkan data baru seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/diklat_tambah.png')" alt="TampilanDiklatTambah">
</kbd>

Kemudian tekan tombol `Update` pada gambar diatas, untuk mengkonfirmasi agar data tersimpan ke server.

## Ubah Data Diklat

Pastikan pilih baris data yang akan di ubah pada layar _Grid_, lalu pada layar/tampilan _Grid_ Diklat terletak di bagian kiri atas, tekan tombol `Ubah` lalu akan muncul kolom ubah pada baris data yang sudah dipilih untuk melakukan perubahan data seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/diklat_ubah.png')" alt="TampilanDiklatUbah">
</kbd>

Kemudian tekan tombol `Update` pada gambar diatas, untuk mengkonfirmasi agar data tersimpan ke server.


## Unggah (Upload) Data Diklat

Pastikan pilih baris data yang akan di tambahkan lampiran pada layar _Grid_, lalu pada layar/tampilan _Grid_ Diklat terletak di bagian kanan atas, tekan tombol `Upload` lalu akan muncul tampilan upload data yang akan dilampirkan pada untuk menambahkan data baru seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/diklat_upload.png')" alt="TampilanDiklatUpload">
</kbd>

Kemudian tekan tombol `Simpan` pada gambar diatas, untuk mengkonfirmasi agar data lampiran tersimpan ke server atau tekan tombol `Batal` untuk membatalkan penambahan lampiran.

## Hapus Data Diklat

Pastikan pilih baris data yang akan di hapus pada layar _Grid_, lalu pada layar/tampilan _Grid_ Diklat terletak di bagian kanan atas, tekan tombol `Hapus` lalu akan muncul layar konfirmasi untuk menghapus data yang telah dipilih, seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/diklat_hapus.png')" alt="TampilanDiklatHapus">
</kbd>

Kemudian tekan tombol `Ya` pada gambar diatas, untuk mengkonfirmasi agar data terhapus dari server atau tekan tombol `Tidak` untuk membatalkan proses penghapusan data.