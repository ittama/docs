---
title: Pencatatan & Distribusi - TU
date: 2020-1-29
tags:
  - pencatatan
  - distribusi
  - login
author: Torina
featuredimg: "https://images.unsplash.com/photo-1482876555840-f31c5ebbff1c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1351&q=80"
summary: For a moment she wondered how she had happened to wake so early.
---

# Pencatatan & Distribusi - TU

## Login <Badge text="Operator TU Settama"/>

Akses halaman aplikasi menggunakan browser ke alamat [Aplikasi Sindie](http://torina.id/sindie)

## Akses Menu Pencatatan

Untuk mengakses halaman pencatatan surat baru, silahkan arah kursor ke **menu Start** yang terletak di sudut kiri bawah layar, lalu pilih menu **Pencatatan**, seperti berikut :

![MenuPencatatan](/assets/img/nm/menu_pencatatan.min.png =200x)

Akan tampil grid _data Pencatatan Surat_ berbentuk tabel yang didalamnya terdapat fitur _filtering data_ berdasarkan _Header Tabel_

![GridPencatatan](/assets/img/nm/grid_pencatatan.min.png)

## Pencatatan Naskah / Surat

Setelah berada dalam _Grid Table_, untuk melakukan Pencatatan Baru, arahkan kursor ke tombol **Pencatatan Baru** di bagian kiri atas layar _Grid Table_, seperti berikut :

![TombolCatatBaru](/assets/img/nm/tombol_catat-baru.min.png =350x)

### Keamanan dan Agenda Naskah

![AgendaNaskah](/assets/img/nm/form_catat-Agenda.min.png =400x)

Keamanan Naskah dibagi menjadi 3 jenis yaitu **Biasa (B)**, **Rahasia (R)** dan **Sangat Rahasia (SR)**

::: details Pengertian Keamanan Naskah

1. **Biasa (B)**
   Tingkat keamanan surat dinas bersifat tidak rahasia, contoh: _Nota Dinas, Surat Undangan, Laporan Perjalanan Dinas, Surat Tugas_

1. **Rahasia (R)**
   Tingkat keamanan surat dinas bersifat rahasia, biasanya berisikan surat dinas yang berhubungan erat dengan keamanan dan keselamatan negara dan instansi. Sifat Rahasia diberikan kepada Naskah Dinas/Dokumen yang isinya memuat keterangan bersifat rahasia, tetapi harus diproses/diketahui oleh beberapa pejabat tertentu secara terbatas dalam kedinasan. contoh: _Berita Acara Operasi Narkoba, Hasil Uji Lab, Surat Teguran, dll_

1. **Sangat Rahasia (SR)**
   Tingkat keamanan surat dinas bersifat sangat rahasia, biasanya berisikan surat dinas yang tertinggi, sangat erat hubungannya dengan keamanan dan keselamatan negara serta instansi. Jika disiarkan secara tidak sah atau jatuh ke tangan yang tidak berhak, surat ini akan membahayakan keamanan dan keselamatan negara. contoh: _Hasil Pemeriksaan, Data dan Informasi Jaringan Narkotika_

\*_`berdasarkan PERBNN Nomor 1 Tahun 2018 ttg Tata Naskah Dinas`_
:::

::: tip Penomoran Nomor Agenda
**Nomor Agenda** akan muncul setelah status surat sudah didistribusikan oleh Operator Tata Usaha Settama BNN, dalam status pencatatan Nomor agenda masih dikosongkan
:::

### Input Detail Naskah Masuk

![DetailNaskah](/assets/img/nm/form_catat-DetailNaskah.min.png)

Isikan inputan detail naskah masuk diatas, dengan isian sebagai berikut :

| Field_Detail_Naskah | Isian                                                                                                                                                                                                              |
| :------------------ | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `Nomor Naskah`      | Isikan nomor naskah dengan nomor surat masuk yang telah diterima                                                                                                                                                   |
| `Tanggal Naskah`    | Isikan tanggal naskah sesuai dengan nomor surat masuk                                                                                                                                                              |
| `Tanggal Terima`    | secara default tanggal terima terisi dengan tanggal hari surat masuk diterima                                                                                                                                      |
| `Sifat Naskah`      | Pilih sifat naskah sesuai dengan tingkat kecepatan penyampaian surat masuk yaitu Biasa, Segera atau Kilat (Sangat Segera)                                                                                          |
| `Status Berkas`     | Pilih status berkas sesuai dengan Surat Masuk yang diterima merupakan surat Asli, Copy, atau tembusan, untuk status amplop tertutup ceklist dibagian bawah form                                                    |
| `Bentuk Naskah`     | Pilih bentuk naskah berdasarkan bentuk surat, Biasa (surat yang dikirimkan secara langsung/pos/kurir), Fax, Undangan Cetak, Email (surat elektronik), Surat Telegram atau Surat Pengaduan                          |
| `Lampiran`          | Isikan lampiran dengan lampiran berupa jumlah berkas atau dokumen yang disertai dengan surat masuk                                                                                                                 |
| `Amplop Tertutup`   | Ceklist isian ini jika penerimaan merupakan surat Rahasia atau sangat rahasia, yang nantinya setelah diterima oleh pejabat tertuju isian perihal diisikan ulang dan hanya bisa dilihat oleh pejabat berwenang saja |

### Input Asal Naskah / Surat Masuk (Internal) <Badge text="Sementara" type="warning"/>

- Isikan asal Naskah / Surat masuk yang berasal dari internal dengan memilih _Option_ `Internal`, seperti berikut :

  ![AsalNaskahInt-1](/assets/img/nm/form_catat-AsalNaskahInt.min.png)

- Tuliskan sebagian nama jabatan agar pilihan `Jabatan` yang menjadi asal dari Naskah / Surat masuk muncul untuk dijadikan pilihan, seperti berikut :

  ![AsalNaskahInt-2](/assets/img/nm/form_catat-AsalNaskahInt-2.min.png)

- Tuliskan Nama Pejabat terkait pilihan Jabatan diatas (Opsional), pada _field_ `Nama` sebagai berikut :

  ![AsalNaskahInt-3](/assets/img/nm/form_catat-AsalNaskahInt-3.min.png)

::: warning Inputan Asal Naskah Surat Internal ini bersifat sementara
Asal Naskah Surat Internal tidak akan diperlukan lagi jika Proses Surat Keluar untuk internal sudah aktif, sehingga kegiatan pengiriman surat secara internal akan dikirim dan diterima secara langsung di internal.
:::

### Input Asal Naskah / Surat Masuk (External)

- Isikan asal Naskah / Surat masuk yang berasal dari External dengan memilih _Option_ `External`, seperti berikut :

  ![AsalNaskahExt-1](/assets/img/nm/form_catat-AsalNaskahExt.min.png)

- Tuliskan sebagian nama Jenis Instansi agar pilihan `Jenis Instansi` yang menjadi asal dari Naskah / Surat masuk muncul untuk dijadikan pilihan, seperti berikut :

  ![AsalNaskahExt-3](/assets/img/nm/form_catat-AsalNaskahExt-3.min.png)

- Pilih Nama Instansi yang sudah tersimpan atau Tulis Nama Instansi Baru jika di dalam pilihan tersebut belum tersimpan, agar `Nama Instansi` yang menjadi asal dari Naskah / Surat masuk dipilih atau tersimpan, seperti berikut :

  ![AsalNaskahExt-4](/assets/img/nm/form_catat-AsalNaskahExt-4.min.png)

- Tuliskan `Alamat Instansi` yang sudah dipilih atau di input, agar kedepan dapat digunakan jika Naskah / Surat masuk berasal dari instansi yang sama

- Tuliskan juga `Nama Pengirim` jika diperlukan

  ![AsalNaskahExt-5](/assets/img/nm/form_catat-AsalNaskahExt-5.min.png)

### Input Tujuan Naskah / Surat Masuk

- Tujuan Naskah / Surat Masuk di isikan sesuai dengan surat yang diterima, ditujukan untuk satu SATKER atau beberapa SATKER sekaligus, sebagai berikut :

  ![TujuanNaskah-1](/assets/img/nm/form_catat-TujuanNaskah.min.png)

- Jika tujuannya naskah / surat untuk beberapa SATKER sekaligus maka pilih _Option_ SATKER/SUB SATKER/BAGIAN/SUB BAGIAN, lalu tuliskan sebagian nama SATKER yang di tuju lalu pilih tujuan dari _Dropdown Menu_ yang muncul, sebagai berikut :

  ![TujuanNaskah-2](/assets/img/nm/form_catat-TujuanNaskah-2.min.png)

- Pilih beberapa SATKER tujuan sesuai dengan tujuan yang ada pada Naskah / Surat masuk yang diterima :

  ![TujuanNaskah-3](/assets/img/nm/form_catat-TujuanNaskah-3.min.png)

- Pastikan Isian tujuan sudah sesuai, lalu tekan tombol _Simpan_ untuk melakukan proses penyimpanan

  ![TujuanNaskah-5](/assets/img/nm/form_catat-TujuanNaskah-5.min.png)

## Lampiran

### Input Lampiran Naskah / Surat Masuk

Lampiran merupakan berkas _hardcopy_ dari Naskah / Surat yang diterima, agar dapat menjadi arsip yang disimpan dan dapat dicari menggunakan _Detail Naskah_ yang sudah diinputkan

::: warning Lampiran Naskah Masuk Internal
Lampiran Naskah Masuk (Internal) merupakan inputan secara manual yang bersifat sementara, jika Fitur Naskah Keluar sudah aktif, proses surat keluar internal akan langsung dikirimkan ke SATKER / Pejabat sebagai Naskah Masuk
:::

Lampiran dapat diisikan pada _Tab_ `File Naskah Masuk`, setelah Proses _Pencatatan_ atau _Pencatatan Langsung_ yang telah di _Simpan_ terlebih dahulu, lalu kembali ke Naskah tersebut dengan menggunakan _Menu_ `Ubah Pencatatan`

![TombolCatatUbah](/assets/img/nm/tombol_catat-ubah.min.png =350x)

- Arahkan _Cursor_ ke _Menu_ `File Naskah Masuk`, lalu tekan tombol `+ Tambah` untuk memulai melakukan penambahan _File_ / Berkas Surat Masuk

  ![FileNaskah-1](/assets/img/nm/form_catat-FileNaskah.min.png)

- Akan muncul _Window_ isian _File_ / berkas yang akan di Unggah, untuk mengunggah tekan tombol `Upload..`

  ![FileNaskah-2](/assets/img/nm/form_catat-FileNaskah-2.min.png)

- _Window_ untuk melakukan pencarian _File_ akan muncul, lalu cari dan pilih file yang akan di unggah (pastikan berkas sudah disiapkan sebelumnya dengan proses _Scan_ atau dengan mengamnbil gambar melalui perangkat _Mobile_)

  ![FileNaskah-3](/assets/img/nm/form_catat-FileNaskah-3.min.png)

- Nama _File_ yang dipilih akan muncul pada isian _File_ / berkas yang akan di unggah, lalu tekan tombol `Simpan` atau tombol `Tutup` jika ingin membatalkan proses unggah _File_ lampiran

  ![FileNaskah-4](/assets/img/nm/form_catat-FileNaskah-4.min.png)

- Pada _Grid_ `File Naskah Masuk` akan muncul _File_ / berkas yang sudah berhasil di unggah dan di simpan

  ![FileNaskah-5](/assets/img/nm/form_catat-FileNaskah-5.min.png)

- Untuk melihat dan memastikan _File_ yang diunggah sudah sesuai, tampilkan _File_ dengan mengarahkan _cursor_ ke tombol berikut, tombol di sebelah kiri adalah tombol `Tampilkan` dan yang lain adalah tombol `Unduh`

  ![TombolCatatUbah](/assets/img/nm/tombol_catat-FileTampilkan.min.png =450x)

- Contoh bentuk berkas yang ditampilkan adalah sebagai berikut

  ![FileNaskah-6](/assets/img/nm/form_catat-FileNaskah-6.min.png)

- Proses `Ubah` untuk _File_ / berkas yang telah di unggah, sama dengan proses pada saat mengunggah baru

  ![FileNaskah-7](/assets/img/nm/form_catat-FileNaskah-7.min.png)

- Proses `Hapus` jika _File_ yang di unggah sudah tidak diperlukan

  ![FileNaskah-8](/assets/img/nm/form_catat-FileNaskah-8.min.png)

## Klasifikasi Arsip

Klasifikasi Arsip adalah pengelompokan dokumen (arsip) menurut permasalahan dari seluruh proses kegiatan yang dilakukan oleh instansi (unit kerja) dalam rangka melaksanakan tugas dan fungsinya.
Pengelompokan dimaksud dilakukan secara sistematis dan logis serta berjenjang dengan diberi tanda-tanda khusus yang berfungsi sebagai kode

### Input Klasifikasi Arsip untuk Naskah / Surat Masuk

- Arahkan _cursor_ ke _Tab_ `Klasifikasi Arsip`
  ![KlasifNaskah-1](/assets/img/nm/form_catat-KlasifNaskah.min.png)

- Sesuaikan klasifikasi menurut Naskah / Surat Masuk yang telah diterima

  ![KlasifNaskah-2](/assets/img/nm/form_catat-KlasifNaskah-2.min.png)

- Setelah penentuan klasifikasi selesai, tekan tombol `Simpan`

  ![KlasifNaskah-3](/assets/img/nm/form_catat-KlasifNaskah-3.min.png)

## Ubah Pencatatan
