---
title: Profil Pegawai
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# PROFIL PEGAWAI

Profil pegawai merupakan data dasar yang dimiliki oleh pegawai seperti NIP/NRP, Tempat dan Tanggal Lahir, Nomor Telepon, Alamat Email, Alamat Domisili, Nomor KTP, TMT, Pendidikan Terakhir, Satuan Kerja sampai dengan Jabatan, Pangkat dan Golongan. Data-data tersebut akan dibandingkan dengan data dari Kepegawaian (_Jika Tersinkronisasi_)

<img :src="$withBase('/assets/img/nm/view_profil.png')" alt="TampilanViewProfil">

## Ubah Gambar Profil

Untuk melakukan perubahan Gambar/Photo pada Profil, dapat melakukan mengakses tombol `Upload` pada bagian kanan atas, lalu pilih gambar yang di inginkan

<img :src="$withBase('/assets/img/nm/profil_view.png')" alt="TampilanProfilView">

## Ubah Data Profil

Untuk mengubah data pada profil pegawai bersangkutan, masukan atau ubah data yang sudah ada dengan data perubahan yang diinginkan

<img :src="$withBase('/assets/img/nm/profil_edit.png')" alt="TampilanProfilEdit">

Lalu tekan tombol `Simpan` untuk mengkonfirmasi perubahan atau penambahan data.
