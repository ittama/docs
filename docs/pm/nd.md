---
title: Nota Dinas Diklat
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# NOTA DINAS DIKLAT

Nota Dinas Pendidikan dan Pelatihan merupakan proses mengikuti Pendidikan dan Pelatihan di lingkungan Inspektorat Utama BNN.
- Diawali dengan proses pemberitahuan untuk mengikuti Diklat dari pihak penyelenggara, dengan melakukan pengisian data kriteria serta persyaratan untuk mengikuti pelatihan pada _menu_ **ND Permintaan** 
- Kemnudian Sub Satker di Inspektorat Utama yang akan mengajukan pegawai/anggota untuk mengikuti Diklat,  dengan melakukan proses pengisian data pengajuan pegawai serta menyesuaikan kriteria dan persyaratan yang dipersyaratkan untuk mengikuti Diklat pada _menu_ **ND Pengajuan**,
- Setelah proses diatas, operator / admin akan melakukan proses Verifikasi Data, jika terdapat kekurangan data Operator akan mengirimkan Notifikasi / pemberitahuan mengenai Data/Dokumen yang belum lengkap. pada _menu_ **ND Pengajuan (Verifikasi)** 

<kbd>
  <img :src="$withBase('/assets/img/pm/ndpermintaan_view.png')" alt="TampilanNDPermintaanView">
</kbd>

## Tambah Data ND Permintaan

Untuk menambahkan ND Permintaan, pilih atau tekan tombol `Tambah Data` pada bagian kiri atas layar _Grid_ ND Permintaan, lalu akan muncul layar seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/pm/ndpermintaan_add.png')" alt="TampilanNDPermintaanAdd">
</kbd>

Isikan inputan detail naskah masuk diatas, dengan isian sebagai berikut :

Field Label                     | Isian 
--  | - |
`Tanggal` | Tanggal ND Permintaan, sesuaikan dengan Dokumen Nota Dinas <Badge text="wajib diisikan" type="error" /> 
`Klasifikasi_Arsip`    | Cari atau Pilih Klasifikasi Arsip dari Nota Dinas yang akan  di simpan <Badge text="wajib diisikan" type="error" />
`Nomor`    | Setelah memilih isian Klasifikasi Arsip di atas, _template_ nomor akan muncul pada isian nomor, silahkan mengisikan nomor yang di inginkan pada field isian di sebelah kiri _field template_ penomoran  <Badge text="wajib diisikan" type="error"/>  
`Nama_Diklat`   | Isikan Nama Diklat sesuai dengan Nota Dinas <Badge text="wajib diisikan" type="error"/>
`Tanggal_Pelaksanaan`   | Isikan dengan tanggal mulai pelaksanaan dan selesai pelaksanaan Diklat sesuai dengan Nota Dinas <Badge text="wajib diisikan" type="error" /> 
`Tempat`   | Isikan dengan lokasi pelaksanaan Diklat sesuai dengan Nota Dinas 
`Kriteria`   | Isikan dengan lengkap Kriteria dan Persyaratan yang harus dipenuhi oleh pegawai untuk dapat mengikuti Diklat tersebut
`Batas_Waktu_Pengajuan`   | Isikan dengan tanggal batas waktu untuk menerima pengajuan dari Sub-Satker
`Nama_File`   | Unggah file Nota Dinas dengan menekan tombol upload pada bagian kanan _field_
`Kirim_Notifikasi`   | Jika _Checkbox_ ini di _checklist_, Pemberitahuan akan dikirimkan ke seluruh pegawai Inspektorat Utama

## ND Pengajuan

Data **ND Pengajuan**, diisikan berdasarkan **ND Permintaan** oleh Operator/Admin yang bertanggungjawab (Sepri Inspektur atau yang ditunjuk) dengan melampirkan Nota Dinas Pengajuan serta daftar nama pegawai yang akan diajukan untuk mengikuti pelatihan. 

<kbd>
  <img :src="$withBase('/assets/img/pm/ndpengajuan_view.png')" alt="TampilanNDPengajuanView">
</kbd>

## Tambah Data ND Pengajuan

<kbd>
  <img :src="$withBase('/assets/img/pm/ndpengajuan_add.png')" alt="TampilanNDPengajuanAdd">
</kbd>

**Pencarian Data Pegawai**
<kbd>
  <img :src="$withBase('/assets/img/pm/ndpengajuan_add_pegawai.png')" alt="TampilanNDPengajuanAddPegawai">
</kbd>

**Tambah Data Pegawai**
<kbd>
  <img :src="$withBase('/assets/img/pm/ndpengajuan_daftarnamapegawai.png')" alt="TampilanNDPengajuanDaftarNamaPegawai">
</kbd>

Field Label                     | Isian 
--  | - |
`ND Permintaan` | Nomor ND Permintaan sebagai dasar ND Pengajuan Diklat  ![Input Otomatis !](https://img.shields.io/badge/Input-Otomatis-1abc9c.svg)
`Tanggal` | Tanggal ND Permintaan, sesuaikan dengan Dokumen Nota Dinas <Badge text="wajib diisikan" type="error" /> 
`Klasifikasi_Arsip`    | Cari atau Pilih Klasifikasi Arsip dari Nota Dinas yang akan  di simpan <Badge text="wajib diisikan" type="error" />
`Nomor`    | Setelah memilih isian Klasifikasi Arsip di atas, _template_ nomor akan muncul pada isian nomor, silahkan mengisikan nomor yang di inginkan pada field isian di sebelah kiri _field template_ penomoran  <Badge text="wajib diisikan" type="error"/>  
`Nama_File`   | Unggah file Nota Dinas dengan menekan tombol upload pada bagian kanan _field_
`Jumlah_Orang`   | Masukan jumlah pegawai yang akan diajukan untuk mengikuti Diklat
`Daftar Nama`   | Grid/Tabel Daftar Nama pegawai yang diajukan, untuk melakukan penambahan Data Pegawai tekan tombol `Tambah Data`, lalu Pilih Nama Pegawai yang akan diikutkan, kemudian tekan tombol Update untuk melakukan penyimpanan data
`Kirim_untuk_Verifikasi`   | Jika _Checkbox_ ini di _checklist_, Dokumen akan dikirimkan ke Operator Verifikasi data pengajuan, jika tidak, Dokumen hanya akan disimpan saja