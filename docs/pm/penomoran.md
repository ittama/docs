---
title: Penomoran Laporan
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# PENOMORAN LAPORAN

Penomoran Laporan berfungsi untuk mendapatkan terlebih dahulu Nomor Laporan sehingga pada saat laporan tersebut akan di unggah/upload Nomor sudah tersedia dan sesuai dengan tanggal kegiatan di dalam surat perintah (Sprin)

<kbd>
  <img :src="$withBase('/assets/img/pm/nomor_view.png')" alt="TampilanPenomoranView">
</kbd>

## Tambah Data Penomoran

Untuk menambahkan penomoran Laporan, pilih atau tekan tombol `Tambah` pada bagian kiri atas layar _Grid_ penomoran laporan, lalu akan muncul layar seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/pm/nomor_add.png')" alt="TampilanPenomoranTambah">
</kbd>

Isikan inputan detail naskah masuk diatas, dengan isian sebagai berikut :

| Field_Detail | Isian                                                                                                                                                                                                              |
| :------------------ | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `Nomor Sprin - Judul`      | Pilih dan sesuaikan nomor sprin yang akan dimintakan nomor Laporan, seluruh nomor yang menjadi pilihan adalah semua Sprin yang memang memerlukan _feedback_ laporan <Badge text="wajib diisikan" type="error"/>                                                                                                                                                    |
| `Nomor Laporan`    | Isian Nomor laporan akan langsung terisi setelah field diatas (Nomor Sprin - Judul) di pilih <Badge text="wajib diisikan" type="error"/>                                                                                                                                                             |
| `Kepada`    | Pilih nama-nama pegawai yang tercantum di dalam Sprin yang akan di Notifikasi seperti yang menjabat Ketua, Wakil Ketua, dan/atau Sekretaris <Badge text="wajib diisikan" type="error"/>  |
| `Kirim Notifikasi`   | Ceklist `Kirim Notifikasi` untuk memberikan notifikasi kepada daftar nama-nama orang/personil yang telah di input pada _Grid_ Daftar Nama diatas |
| `Simpan`   | Tekan Tombol `Simpan`, untuk mengkonfirmasi bahwa data Sprin di atas sudah benar dan akan di simpan ke server, jika `Kirim Notifikasi` di Ceklist, proses pengiriman notifikasi akan langsung dijalankan setelah tombol `Simpan` ditekan |
| `Tutup`   | Tekan Tombol `Tutup`, untuk membatalkan semua perubahan isian atau penambahan data Sprin |

## Ubah Data Penomoran Laporan

Untuk menambah data Sprin, pastikan memilih data pada _Grid_ terlebih dahulu kemudian Tekan tombol `Ubah` pada bagian kiri atas _Grid_ data Sprin, akan muncul halaman isian data sprin untuk diubah seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/pm/sprin_added.png')" alt="TampilanSprinUbah">
</kbd>

## Unggah (Upload) File Laporan <Badge text="Umum" type="warning"/>

Pastikan pilih baris data yang akan di tambahkan lampiran pada layar _Grid_, lalu pada layar/tampilan _Grid_  terletak di bagian kanan atas, tekan tombol `Upload` lalu akan muncul tampilan upload data yang akan dilampirkan pada untuk menambahkan data baru seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/diklat_upload.png')" alt="TampilanDiklatUpload">
</kbd>

Kemudian tekan tombol `Simpan` pada gambar diatas, untuk mengkonfirmasi agar data lampiran tersimpan ke server atau tekan tombol `Batal` untuk membatalkan penambahan lampiran.

## Hapus Data <Badge text="Umum" type="warning"/>

Pastikan pilih baris data yang akan di hapus pada layar _Grid_, lalu pada layar/tampilan _Grid_  terletak di bagian kanan atas, tekan tombol `Hapus` lalu akan muncul layar konfirmasi untuk menghapus data yang telah dipilih, seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/diklat_hapus.png')" alt="TampilanDiklatHapus">
</kbd>

Kemudian tekan tombol `Ya` pada gambar diatas, untuk mengkonfirmasi agar data terhapus dari server atau tekan tombol `Tidak` untuk membatalkan proses penghapusan data.

## Kirim Ulang Notifikasi <Badge text="Umum" type="warning"/>

Untuk mengirim ulang notifikasi, cukup memilih data Sprin pada daftar _Grid_, lalu tekan tombol `Notifikasi` terletak pada bagian kanan atas _Grid_, akan muncul tampilan sebagai berikut :

<kbd>
  <img :src="$withBase('/assets/img/pm/sprin_notif.png')" alt="TampilanSprinNotif">
</kbd>

Kemudian tekan tombol `Ya` pada gambar diatas, untuk mengkonfirmasi agar data notifikasi terkirim ke daftar nama tersebut diatas atau tekan tombol `Tidak` untuk membatalkan proses pengiriman notifikasi.
