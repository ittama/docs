---
title: Perjalanan Dinas
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# SURAT PERJALANAN DINAS

Laporan perjalanan Dinas sama halnya dengan Penomoran Laporan, pada saat Sprin di inputkan dan pilihan SPD di ceklist, isian laporan perjalanan dinas akan terdapat pilihan Sprin yang sudah di input tersebut didalan inputan data laporan perjalanan dinas. Terdapat 4 bagian SPD dalam modul ini, sebagai berikut :
- **SPD Jalan**, merupakan SPD pengajuan dan belum direalisasikan serta masih mengalami kemungkinan perubahan data penerima Sprin, tetapi tetap harus di data dalam Laporan Perjalanan Dinas
- **SPD Rampung**, Merupakan SPD yang sudah direalisasikan dan memiliki status sudah _Fix_ dan tidak akan mengalami perubahan data penerima berdasarkan data yang dilaporkan.
- **SPD Diklat**, Merupakan SPD yang dikelompokan hanya perjalanan dinas untuk pendidikan dan pelatihan saja, tanpa memiliki pengeluaran untuk Penginapan dan Transportasi.
- **SPD Transport**, Merupakan SPD yang dikelompokkan hanya perjalanan dinas yang jenis pengeluarannya hanya Tansportasi saja.

<kbd>
  <img :src="$withBase('/assets/img/pm/spd_view.png')" alt="TampilanSPDView">
</kbd>

pada menu _Grid_ dapat melihat detil data SPD dan List data SPD yang telah di `Generate`, seperti berikut :

**List Data SPD**
<kbd>
  <img :src="$withBase('/assets/img/pm/spd_list.png')" alt="TampilanSPDList">
</kbd>

**Detail Data SPD**
<kbd>
  <img :src="$withBase('/assets/img/pm/spd_detail.png')" alt="TampilanSPDDetail">
</kbd>


## Tambah Data SPD <Badge text="SPD Jalan dan Rampung" type="warning"/>

Penambahan data SPD akan muncul jika pada saat pengisian data SPrin Baru, pada bagian SPD di ceklist seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/pm/spd_add.png')" alt="TampilanSPDTambah">
</kbd>

## Generate Data SPD <Badge text="SPD Jalan dan Rampung" type="warning"/>

Untuk menambahkan data laporan Perjalanan Dinas, SPD harus terlebih dahulu di Generasi (`Generate`), tekan tombol `Generate SPD` dibagian kanan atas layar _Grid_, lalu akan muncul tampilan layar seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/pm/spd_generate.png')" alt="TampilanSPDGenerate">
</kbd>

Kolom `Status` pada tampilan _Grid_ data Sprin akan berubah menjadi `Generate`

## Ubah Data SPD <Badge text="SPD Jalan dan Rampung" type="warning"/>

Syarat untuk melakukan perubahan data Perjalanan Dinas adalah 
- Masih dalam Status `Generate`
- Masih dalam Status `Ubah`
- Status masih belum `Selesai`

untuk melakukan perubahan data spd tekan Tombol `Ubah SPD` pada bagian kiri atas _Grid_ SPD, lalu akan muncul layar sebagai berikut :

<kbd>
  <img :src="$withBase('/assets/img/pm/spd_edit.png')" alt="TampilanSPDUbah">
</kbd>

Seluruh isian sudah merupakan isian otomastis berdasarkan isian Sprin sebelumnya.
Untuk mengubah data dalam _Grid_ SPD, Tekan tombol `Ubah Data` pada bagian kiri atas layar _Grid_ SPD lalu akan muncul tampilan seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/pm/spd_edit2.png')" alt="TampilanSPDUbahGrid">
</kbd>


## Hapus Data <Badge text="Umum" type="warning"/>

Pastikan pilih baris data yang akan di hapus pada layar _Grid_, lalu pada layar/tampilan _Grid_  terletak di bagian kanan atas, tekan tombol `Hapus` lalu akan muncul layar konfirmasi untuk menghapus data yang telah dipilih, seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/diklat_hapus.png')" alt="TampilanDiklatHapus">
</kbd>

Kemudian tekan tombol `Ya` pada gambar diatas, untuk mengkonfirmasi agar data terhapus dari server atau tekan tombol `Tidak` untuk membatalkan proses penghapusan data.
