---
title: Surat Perintah/Penugasan
date: 2021-1-02
tags:
  - pustaka
  - profil
  - pegawai
author: Torina
featuredimg: ""
summary: For a moment she wondered how she had happened to wake so early.
---

# SURAT PERINTAH (SPRIN)/PENUGASAN

Surat Perintah (Sprin) / Penugasan ini diharapkan dapat memperlancar distribusi dan pemberitahuan kepada para pegawai yang mendapatkan tugas via notifikasi yang akan dikirimkan melalui perangkat android (Mobile)

<kbd>
  <img :src="$withBase('/assets/img/pm/sprin_view.png')" alt="TampilanSprinView">
</kbd>

## Tambah Data Sprin

Untuk menambah data Sprin, Tekan tombol `Tambah` pada bagian kiri atas _Grid_ data Sprin, akan muncul halaman isian data sprin baru seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/pm/sprin_add.png')" alt="TampilanSprinTambah">
</kbd>

Isikan inputan detail naskah masuk diatas, dengan isian sebagai berikut :

| Field_Detail | Isian                                                                                                                                                                                                              |
| :------------------ | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `Klasifikasi Arsip`      | Pilih dan sesuaikan sprin dengan klasifikasi arsip <Badge text="wajib diisikan" type="error"/>                                                                                                                                                    |
| `Tanggal`    | Isikan tanggal Sprin <Badge text="wajib diisikan" type="error"/>                                                                                                                                                             |
| `Nomor`    | Isikan nomor Sprin yang diterima <Badge text="wajib diisikan" type="error"/> <img :src="$withBase('/assets/img/pm/sprin_nomor.png')" alt="TampilanSprinNomor"> Isikan hanya nomor Sprin saja pada _field_ 1, dan Jenis penomorannya di _field_ 2                                                                                                                                       |
| `Judul`      | Isikan Judul pada Sprin yang diterima <Badge text="wajib diisikan" type="error"/>                                                                                         |
| `Tanggal Pelaksanaan`     | Masukan tanggal mulai pelaksanaan kegiatan dan tanggal selesai kegiatan pada 2 _field_ yang tersedia, dapat menggunakan _date-picker_ atau dengan menuliskan manual tanggal yang diinginkan sesuai format                                                   |
| `Laporan`     | Ceklist pada Laporan jika Sprin ini membutuhkan _feedback_ berupa Laporan Kegiatan, sehingga Penomoran Laporan akan segera disediakan                          |
| `SPD`          | Ceklist pada SPD jika kegiatannya harus ke luar daerah, sehingga _feedback_ Laporan perjalanan dinas akan disediakan                                                                                                                 |
| `Provinsi`   | menu Provinsi ini akan otomatis muncul jika SPD di ceklist, Pilih daerah tujuan pelaksanaan kegiatan sesuai dengan Sprin <Badge text="wajib diisikan" type="error"/>|
| `Lokasi`   | Isikan lokasi pelaksanaan kegiatan sesuai dengan Sprin <Badge text="wajib diisikan" type="error"/>|
| `Nama File`   | Unggah file Sprin sebagai lampiran dari dokumen Sprin yang disimpan <Badge text="wajib diisikan" type="error"/>|
| `Jumlah Orang`   | Isikan jumlah orang yang tertera pada Sprin, agar isian pada _Grid_ dibawahnya akan tersedia sesuai dengan jumlah orang yang diisikan <Badge text="wajib diisikan" type="error"/>|
| `Daftar Nama`   | Isikan Nama-nama orang yang tertera pada Sprin, pada isian pada _Grid_ dengan menekan tombol `Tambah` pada bagian atas _Grid_, lalu isikan data sesuai dengan kolom pada isian _Grid_ <Badge text="wajib diisikan" type="error"/>|
| `Kirim Notifikasi`   | Ceklist `Kirim Notifikasi` untuk memberikan notifikasi kepada daftar nama-nama orang/personil yang telah di input pada _Grid_ Daftar Nama diatas |
| `Simpan`   | Tekan Tombol `Simpan`, untuk mengkonfirmasi bahwa data Sprin di atas sudah benar dan akan di simpan ke server, jika `Kirim Notifikasi` di Ceklist, proses pengiriman notifikasi akan langsung dijalankan setelah tombol `Simpan` ditekan |
| `Tutup`   | Tekan Tombol `Tutup`, untuk membatalkan semua perubahan isian atau penambahan data Sprin |


## Ubah Data Sprin

Untuk menambah data Sprin, pastikan memilih data pada _Grid_ terlebih dahulu kemudian Tekan tombol `Ubah` pada bagian kiri atas _Grid_ data Sprin, akan muncul halaman isian data sprin untuk diubah seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/pm/sprin_added.png')" alt="TampilanSprinUbah">
</kbd>

## Unggah (Upload) Data <Badge text="Umum" type="warning"/>

Pastikan pilih baris data yang akan di tambahkan lampiran pada layar _Grid_, lalu pada layar/tampilan _Grid_  terletak di bagian kanan atas, tekan tombol `Upload` lalu akan muncul tampilan upload data yang akan dilampirkan pada untuk menambahkan data baru seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/diklat_upload.png')" alt="TampilanDiklatUpload">
</kbd>

Kemudian tekan tombol `Simpan` pada gambar diatas, untuk mengkonfirmasi agar data lampiran tersimpan ke server atau tekan tombol `Batal` untuk membatalkan penambahan lampiran.

## Hapus Data <Badge text="Umum" type="warning"/>

Pastikan pilih baris data yang akan di hapus pada layar _Grid_, lalu pada layar/tampilan _Grid_  terletak di bagian kanan atas, tekan tombol `Hapus` lalu akan muncul layar konfirmasi untuk menghapus data yang telah dipilih, seperti berikut :

<kbd>
  <img :src="$withBase('/assets/img/nm/diklat_hapus.png')" alt="TampilanDiklatHapus">
</kbd>

Kemudian tekan tombol `Ya` pada gambar diatas, untuk mengkonfirmasi agar data terhapus dari server atau tekan tombol `Tidak` untuk membatalkan proses penghapusan data.

## Kirim Ulang Notifikasi <Badge text="Umum" type="warning"/>

Untuk mengirim ulang notifikasi, cukup memilih data Sprin pada daftar _Grid_, lalu tekan tombol `Notifikasi` terletak pada bagian kanan atas _Grid_, akan muncul tampilan sebagai berikut :

<kbd>
  <img :src="$withBase('/assets/img/pm/sprin_notif.png')" alt="TampilanSprinNotif">
</kbd>

Kemudian tekan tombol `Ya` pada gambar diatas, untuk mengkonfirmasi agar data notifikasi terkirim ke daftar nama tersebut diatas atau tekan tombol `Tidak` untuk membatalkan proses pengiriman notifikasi.